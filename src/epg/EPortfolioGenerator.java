/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg;

import epg.file.epgFileManager;
import epg.view.ePortfolioGeneratorView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Marisa
 */
public class EPortfolioGenerator extends Application {
    
    epgFileManager fileManager = new epgFileManager();
    ePortfolioGeneratorView ui = new ePortfolioGeneratorView(fileManager);
    
    @Override
    public void start(Stage primaryStage) {
       
        ui.startUI(primaryStage, "ePortfolio Generator");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
