package epg.dialogue;

import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.ICON_HEADER;
import static epg.StartupConstants.ICON_SAVE_EPG;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.controller.FileController;
import epg.error.EPGErrorHandler;
import epg.model.Header;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Marisa DePasquale
 */
public class SaveDialogue extends Stage{
    
    EPGErrorHandler error;
    ePortfolioGeneratorView ui;
    Page selectedPage;
    String initTitle = null;
    TextField inputTitle;
    Label enterTitle;
    Button okButton;
    Button cancelButton;
    VBox vBox;
    HBox headerBox;
    HBox okCancel;
    
    public SaveDialogue(ePortfolioGeneratorView initUI)
    {
      ui = initUI;
      selectedPage = ui.getEPGModel().getSelectedPage();
      initTitle = ui.getEPGModel().getTitle();
      error = new EPGErrorHandler(ui);
      enterTitle = new Label("Name your ePortolio:");  
      inputTitle = new TextField();
      inputTitle.setPromptText("Enter title to save as here");
      okButton = new Button("OK");
      okButton.setMinSize(60, 10);
      cancelButton = new Button("Cancel");
      cancelButton.setMinSize(60, 10);
      
      vBox = new VBox(20); 
      vBox.setMinSize(400, 200);
      vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      
      headerBox = new HBox(5);
      headerBox.setAlignment(Pos.CENTER);
      headerBox.getChildren().add(enterTitle);
      headerBox.getChildren().add(inputTitle);
      
      okCancel = new HBox(100);
      okCancel.setAlignment(Pos.CENTER);
      okCancel.getChildren().add(okButton);
      okCancel.getChildren().add(cancelButton);
      
      vBox.getChildren().add(headerBox);
      vBox.getChildren().add(okCancel);
      
      
      Scene scene = new Scene(vBox);
      scene.getStylesheets().add(STYLE_SHEET_UI);
      this.setTitle("Save");
      String windowImagePath = "file:" + PATH_ICONS + ICON_SAVE_EPG;
      Image windowImage = new Image(windowImagePath); 
      this.getIcons().add((windowImage)); 
      setScene(scene);      
      
      initEventHandlers();  
    }
    
    private void initEventHandlers()
    {

        inputTitle.textProperty().addListener(e -> {
            initTitle = inputTitle.getText();
        }); 
                
                
        cancelButton.setOnAction(e -> {
            hide();
      });
        
        okButton.setOnAction(e -> {
            if(initTitle != null)
            {
                initTitle = inputTitle.getText();
                ui.getEPGModel().setTitle(initTitle);
                hide();
            }
            else
            {
                error.processError("No text was entered to create a title");
            }
                
        });         
    }
}
