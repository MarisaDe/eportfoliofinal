/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.dialogue;

import static epg.StartupConstants.CSS_CLASS_HEADER_VBOX;
import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_BUTTON;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_HBOX;
import static epg.StartupConstants.ICON_ADD_IMAGE;
import static epg.StartupConstants.ICON_SELECT_IMAGE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_SELECT_IMAGE;
import epg.error.EPGErrorHandler;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ssm.controller.ImageCompController;

/**
 *
 * @author Marisa DePasquale
 */
public class ImageDialogue extends Stage{
    
    EPGErrorHandler error;
    ePortfolioGeneratorView ui;
    
    //Displays image for this comp
    epg.view.ImageView imageSelectionView;
    
    //User input dat   
    String initCaption = "";
    String initImageFileName = null;
    String initImagePath = null;
    String initHeight;
    String initWidth;
    String initFloat;          
    Button chooseImageBtn;
    Button okButton;
    Button cancelButton;
    Label imageComp;
    Label file;
    Label addImage;
    Label height;
    Label width;
    Label pos;
    Label caption;
    ComboBox choosePos;
    TextField inputHeight;
    TextField inputWidth;
    TextField inputCaption;
    VBox vBox;
    VBox header;
    HBox chooseImage;
    HBox heightBox; 
    HBox widthBox; 
    HBox floatBox;
    HBox okCancel;
    HBox captionBox;
    
    Page selectedPage;
    
    public ImageDialogue(ePortfolioGeneratorView initUI){
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        initHeight = "default";
        initWidth = "default";

        error = new EPGErrorHandler(ui);
      //  imageSelectionView = new epg.view.ImageView();
        
       //set up all objects
        imageComp = new Label("Choose Image File:");
        addImage = new Label ("Add Image Component");
        addImage.setUnderline(true);
        addImage.getStyleClass().add("CSS_CLASS_HEADER_VBOX");
        
        okButton = new Button("OK");
        okButton.setMinSize(60, 10);
        okButton.getStyleClass().add(CSS_CLASS_OKCANCEL_BUTTON);
        
        
        chooseImageBtn = new Button();
        chooseImageBtn.getStyleClass().add(CSS_CLASS_OKCANCEL_BUTTON);
        
        String imagePath = "file:" + PATH_ICONS + ICON_SELECT_IMAGE;
	Image buttonImage = new Image(imagePath);
	chooseImageBtn.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(TOOLTIP_SELECT_IMAGE);
	chooseImageBtn.setTooltip(buttonTooltip);
        
        
        cancelButton = new Button("Cancel");
        cancelButton.setMinSize(60, 10);
        height = new Label("Height:");
        width = new Label("Width:");
        pos = new Label("Float:");
        caption = new Label("Caption:");
        inputCaption = new TextField();
        inputCaption.setPromptText("Enter Caption Here");
        file = new Label("example.png");  //EXAMPLE
        inputHeight = new TextField();
        inputHeight.setPromptText("Default Height");
        inputWidth = new TextField();
        inputWidth.setPromptText("Default Width");
        
        ObservableList<String> floatChoices = FXCollections.observableArrayList();
	floatChoices.add("Left");
	floatChoices.add("Right");
        floatChoices.add("Neither");
        
        choosePos = new ComboBox(floatChoices);
	choosePos.getSelectionModel().select("Neither");
         
        initEventHandlers();
        
        
        //Contstruct outer box to contain all hboxes
        header = new VBox(10);
        header.getStyleClass().add(CSS_CLASS_HEADER_VBOX);
        vBox = new VBox(5);
        vBox.setMinSize(400, 400);
        vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
        
        header.getChildren().add(addImage);
        
        //Construct the hboxes
        captionBox = new HBox(5);
        chooseImage = new HBox(5);
        floatBox = new HBox(12);
        heightBox = new HBox(5);
        widthBox = new HBox(9);
        okCancel = new HBox(100);
        okCancel.getStyleClass().add(CSS_CLASS_OKCANCEL_HBOX);
               
        captionBox.getChildren().add(caption);
        captionBox.getChildren().add(inputCaption);
        
        chooseImage.getChildren().add(imageComp); 
        chooseImage.getChildren().add(chooseImageBtn);
        chooseImage.getChildren().add(file);   
        
        heightBox.getChildren().add(height);      
        heightBox.getChildren().add(inputHeight); 
        
        widthBox.getChildren().add(width);
        widthBox.getChildren().add(inputWidth);
        
        floatBox.getChildren().add(pos);
        floatBox.getChildren().add(choosePos);

        okCancel.getChildren().add(okButton);
        okCancel.getChildren().add(cancelButton);     
        okCancel.setAlignment(Pos.CENTER);
        
        //add all hboxes to the vbox
        vBox.getChildren().add(header);
        vBox.getChildren().add(chooseImage);
        vBox.getChildren().add(heightBox);
        vBox.getChildren().add(widthBox);
        vBox.getChildren().add(captionBox);
        vBox.getChildren().add(floatBox);   
        vBox.getChildren().add(okCancel);
        
        
        Scene scene = new Scene(vBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setTitle("Add Image Component");
        String windowImagePath = "file:" + PATH_ICONS + ICON_ADD_IMAGE;
        Image windowImage = new Image(windowImagePath); 
        this.getIcons().add((windowImage)); 
        setScene(scene);
        
    }
    
    private void initEventHandlers()
    {  
        ImageCompController controller = new ImageCompController();
        epg.model.Image tempImage = new epg.model.Image();
        chooseImageBtn.setOnAction(e -> {
	    controller.processSelectImage(tempImage);
            initImageFileName = tempImage.getImageFileName();
            initImagePath = tempImage.getImagePath();
            file.setText(initImageFileName);
	});
                
        inputHeight.textProperty().addListener(e -> {
            if(inputHeight.getText().equals(""))
                initHeight = "default";
            else
                initHeight = inputHeight.getText();
        }); 
        
        inputCaption.textProperty().addListener(e -> {
                initCaption = inputCaption.getText();
        }); 
                
                
        //Restrict to only int input
        inputHeight.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.matches("\\d*")) {
                int value = Integer.parseInt(newValue);
            } else {
                inputHeight.setText(oldValue);
            }
        });
        //Restrict to only int input
        inputWidth.textProperty().addListener((ObservableValue<? extends String> observable2, String oldValue2, String newValue2) -> {
            if (newValue2.matches("\\d*")) {
                int value = Integer.parseInt(newValue2);
            } else {
                inputWidth.setText(oldValue2);
            }
        });
        
        inputWidth.textProperty().addListener(e -> {
	    if(inputWidth.getText().equals(""))
                initWidth = "default";
            else
                initWidth = inputWidth.getText();
        });  
            
        cancelButton.setOnAction(e -> {
	    hide();
	});
        
        okButton.setOnAction(e -> {
            if(initImageFileName != null)
            {
                initFloat = choosePos.getSelectionModel().getSelectedItem().toString();
                epg.model.Image createImageComp = new epg.model.Image(initImageFileName, initImagePath, initHeight, initWidth, initFloat, initCaption);
                selectedPage.addExistingImage(createImageComp); 
                ui.getEPGModel().updateContent();
                ui.saveEPGButton.setDisable(false);
                hide();
            }
            else
            {
                error.processError("An image wasn't selected!");
            }
                
        });    
    }
    
}

