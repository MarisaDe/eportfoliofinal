package epg.dialogue;

import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.CSS_CLASS_LIST_PAGES_SCROLL;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_HBOX;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_ADD;
import static epg.StartupConstants.ICON_LIST;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_ADD_LIST_ITEM;
import static epg.StartupConstants.TOOLTIP_REMOVE_LIST_ITEM;
import epg.error.EPGErrorHandler;
import epg.model.List;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Marisa DePasquale
 */
public class ListDialogue extends Stage {
   
    ePortfolioGeneratorView ui;
    EPGErrorHandler error;
    ObservableList<String> listItems;
    ObservableList<TextField> textFieldItems;
    String input;
    TextField inputList;
    Label enterList;
    Button okButton;
    Button cancelButton;
    Button remove;
    Button add;
    VBox vBox;
    VBox listBox;
    HBox okCancel;
    HBox addRemove;
    HBox containPane;
    ScrollPane textBoxes;
    TextField selected;
    TextField inputList2;
    Page selectedPage;
    
    public ListDialogue(ePortfolioGeneratorView initUI)
    {
      ui = initUI;
      selectedPage = ui.getEPGModel().getSelectedPage();
      selected = null;
      error = new EPGErrorHandler(ui);
      inputList2 = new TextField();
      textFieldItems = FXCollections.observableArrayList();
      listItems = FXCollections.observableArrayList();
      enterList = new Label("Enter List Items:");  
      inputList = new TextField();
      inputList.setMinWidth(200);
      inputList.setPromptText("Enter list item here");    
      textFieldItems.add(inputList);
      input = null;
      listItems.add(input);
      okButton = new Button("OK");
      okButton.setMinSize(60, 10);
      cancelButton = new Button("Cancel");
      cancelButton.setMinSize(60, 10);
      
     
      // box to organize adding and removing list items
      addRemove = new HBox(20);
      addRemove.setAlignment(Pos.CENTER);
      addRemove.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      add = initChildButton(addRemove, ICON_ADD,TOOLTIP_ADD_LIST_ITEM,CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON );       
      remove = initChildButton(addRemove, ICON_REMOVE,TOOLTIP_REMOVE_LIST_ITEM,CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON);
      remove.setDisable(true);
      
      //overall container for displaying the dialogue info
      vBox = new VBox(20); 
      vBox.setMinSize(400, 400);
      vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      
      
      //container for the scroll pane so that its CENTER!
      containPane = new HBox();
      containPane.setPrefWidth(380);
      containPane.setPrefHeight(200);
      containPane.setAlignment(Pos.CENTER);
      
      //scroll pane for the textfield to reside
      textBoxes = new ScrollPane();
      textBoxes.getStyleClass().add(CSS_CLASS_LIST_PAGES_SCROLL);
      textBoxes.setPrefWidth(380);
      textBoxes.setMaxHeight(400);
      
      
      //The box to add in the scroll pane which allows for more text fields
      listBox = new VBox(5);
      listBox.setMinWidth(350);
      //listBox.setPrefHeight(400);
      listBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      listBox.setAlignment(Pos.CENTER);
      listBox.getChildren().add(inputList);    
      textBoxes.setContent(listBox);
      containPane.getChildren().add(textBoxes);
      
      //The box that organizes the Ok and Cancel buttons
      okCancel = new HBox(100);
      okCancel.setAlignment(Pos.CENTER);
      okCancel.getStyleClass().add(CSS_CLASS_OKCANCEL_HBOX);       
      okCancel.getChildren().add(okButton);
      okCancel.getChildren().add(cancelButton);
      
      
      //Add all of the boxes/panes to the container
      vBox.getChildren().add(enterList);
      vBox.getChildren().add(addRemove);
      vBox.getChildren().add(containPane);
      vBox.getChildren().add(okCancel);
      
      cancelButton.setOnAction(e -> {
        this.hide();
      });
      
      
      okButton.setOnAction(e -> {
          
        int numOfNull = 0;
        
        for(String string : listItems)
        {
            if(string == null)
            numOfNull++;
        }
        
        if(numOfNull == listItems.size())
        {
          error.processError("Your list must have at least one list item.");  
        }
        else
        {
            List newList = new List(listItems, textFieldItems); 
            selectedPage.addExistingList(newList);
            System.out.println(listItems);
            ui.getEPGModel().updateContent();
            ui.saveEPGButton.setDisable(false);
            this.hide();
        }
      });
      
      inputList.textProperty().addListener(e -> {
          selected = inputList;
          if(inputList.getText().equals(""))
          {
              input = null;
          }
          else
          {
              input = inputList.getText();
          }
            int i = textFieldItems.indexOf(inputList);
            listItems.set(i, input);
      });
      
      
      listBox.onMouseClickedProperty().addListener(e -> {
          selected = null;
      });
              
              
              
      add.setOnAction((ActionEvent e) -> {
        remove.setDisable(false);
        inputList2 = new TextField();
        inputList2.setMinWidth(200);
        inputList2.setPromptText("Enter list item here");
        listBox.getChildren().add(inputList2);
        textFieldItems.add(inputList2);
        String getInput = null; 
        listItems.add(getInput);

        inputList2.textProperty().addListener(y -> {
          
          if(inputList2.getText().equals(""))
          {
              input = null;
          }
          else
          {
              input = inputList2.getText();
          }

          int i = textFieldItems.indexOf(inputList2);
          listItems.set(i, input);  
         });
        
        for(TextField txtItem : textFieldItems)
        {
          txtItem.textProperty().addListener(y -> {
          selected = txtItem;
          if(txtItem.getText().equals(""))
          {
              input = null;
          }
          else
          {
              input = txtItem.getText();
          }
                    
            int i = textFieldItems.indexOf(txtItem);
            listItems.set(i, input);  
         });
        }
       });
    
      
    remove.setOnAction((ActionEvent e) -> {
        int lastIndex = textFieldItems.size()-1;
        textFieldItems.remove(lastIndex);
        listItems.remove(lastIndex);
        refresh();
        if(textFieldItems.size() == 1)
        {
            remove.setDisable(true);
        }
        else
            remove.setDisable(false);
    });    
        
      Scene scene = new Scene(vBox);
      scene.getStylesheets().add(STYLE_SHEET_UI);
      this.setTitle("Add List Component");
      String windowImagePath = "file:" + PATH_ICONS + ICON_LIST;
      Image windowImage = new Image(windowImagePath); 
      this.getIcons().add((windowImage)); 
      setScene(scene);      
    }
    
    
    private void refresh()
      {
        listBox.getChildren().clear();
        int i = 0;
        for(TextField txt : textFieldItems)
        {
            if ( i == 0)
            {
                inputList.setText(listItems.get(0));
                listBox.getChildren().add(inputList);
            }
            
            else{
                TextField reput = new TextField();
                reput.setMinWidth(200);
                reput.setText(listItems.get(i));
                reput.setPromptText("Enter list item here");
                listBox.getChildren().add(reput);
                
          txt.textProperty().addListener(y -> {
            selected = txt;
            String input2 = txt.getText();
            int r = textFieldItems.indexOf(txt);
            listItems.set(r, input2);  
         });
            }
         i++; 
        }
          
      }
          
          
    public final Button initChildButton(
	    HBox hBox, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	hBox.getChildren().add(button);
	return button;
    } 

}

