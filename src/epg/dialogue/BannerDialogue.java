package epg.dialogue;

import static epg.StartupConstants.CSS_CLASS_HEADER_VBOX;
import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_BUTTON;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_HBOX;
import static epg.StartupConstants.ICON_ADD_IMAGE;
import static epg.StartupConstants.ICON_SELECT_IMAGE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_SELECT_IMAGE;
import epg.controller.FileController;
import epg.error.EPGErrorHandler;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageCompController;

/**
 *
 * @author Marisa DePasquale
 */
public class BannerDialogue extends Stage{
    
    EPGErrorHandler error;
    ePortfolioGeneratorView ui;
    
    //Displays image for this comp
    ImageView imageSelectionView = new ImageView();
    
    //User input dat   
    String initImageFileName = "null";
    String initImagePath = "null";        
    Button chooseImageBtn;
    Button okButton;
    Button cancelButton;
    Label imageComp;
    Label addBanner;
    Label fileName;
    Label current;
    VBox vBox;
    VBox header;
    HBox chooseImage;
    HBox okCancel;
    Page selectedPage;
    HBox currentImage;
    
    public BannerDialogue(ePortfolioGeneratorView initUI) throws MalformedURLException{
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        error = new EPGErrorHandler(ui);
        initImagePath = ui.getEPGModel().getBannerPath();
        initImageFileName = ui.getEPGModel().getBannerName();
            
        
       //set up all objects
        imageComp = new Label("Choose Image File:");
        addBanner = new Label ("Add Banner");
        addBanner.setUnderline(true);
        addBanner.getStyleClass().add("CSS_CLASS_HEADER_VBOX");
        
        okButton = new Button("OK");
        okButton.setMinSize(60, 10);
        okButton.getStyleClass().add(CSS_CLASS_OKCANCEL_BUTTON);
        
        
        chooseImageBtn = new Button();
        chooseImageBtn.getStyleClass().add(CSS_CLASS_OKCANCEL_BUTTON);
        
        String imagePath = "file:" + PATH_ICONS + ICON_SELECT_IMAGE;
	Image buttonImage = new Image(imagePath);
	chooseImageBtn.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(TOOLTIP_SELECT_IMAGE);
	chooseImageBtn.setTooltip(buttonTooltip);
        
        
        cancelButton = new Button("Cancel");
        cancelButton.setMinSize(60, 10);

        current = new Label("Current Banner (Will look bigger on webpages):");

        currentImage = new HBox();
        currentImage.setPrefWidth(400);
        currentImage.setAlignment(Pos.CENTER);
        
        if(!initImageFileName.equals("null"))
        {
            File initFile = new File(initImagePath + initImageFileName);
            URL initFileURL = initFile.toURI().toURL();
            Image initBannerImage = new Image(initFileURL.toExternalForm());
            imageSelectionView.setImage(initBannerImage);
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / initBannerImage.getWidth();
	    double scaledHeight = initBannerImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
            currentImage.getChildren().add(imageSelectionView);
        }
        
        
        initEventHandlers();
        
        //Contstruct outer box to contain all hboxes
        header = new VBox(10);
        header.getStyleClass().add(CSS_CLASS_HEADER_VBOX);
        vBox = new VBox(5);
        vBox.setMinSize(400, 400);
        vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
        header.getChildren().add(addBanner);
        
        //Construct the hboxes
        chooseImage = new HBox(5);
        okCancel = new HBox(100);
        okCancel.getStyleClass().add(CSS_CLASS_OKCANCEL_HBOX);

        
        chooseImage.getChildren().add(imageComp); 
        chooseImage.getChildren().add(chooseImageBtn); 
        chooseImage.getStyleClass().add(CSS_CLASS_HEADER_VBOX);
        

        okCancel.getChildren().add(okButton);
        okCancel.getChildren().add(cancelButton);     
        okCancel.setAlignment(Pos.CENTER);
        
        //add all hboxes to the vbox
        
        vBox.getChildren().add(header);
        vBox.getChildren().add(chooseImage);
        vBox.getChildren().add(current);
        vBox.getChildren().add(currentImage);
        vBox.getChildren().add(okCancel);
        
        
        Scene scene = new Scene(vBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        this.setTitle("Add Banner");
        String windowImagePath = "file:" + PATH_ICONS + ICON_SELECT_IMAGE;
        Image windowImage = new Image(windowImagePath); 
        this.getIcons().add((windowImage)); 
        setScene(scene);
        
    }
    
    private void initEventHandlers()
    {  
        ImageCompController controller = new ImageCompController();
        epg.model.Image tempImage = new epg.model.Image();
        
        
        chooseImageBtn.setOnAction(e -> {
	    controller.processSelectImage(tempImage);
            initImageFileName = tempImage.getImageFileName();
            initImagePath = tempImage.getImagePath();
            
  
	
	File file = new File(initImagePath + initImageFileName);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image bannerImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(bannerImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / bannerImage.getWidth();
	    double scaledHeight = bannerImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
            currentImage.getChildren().clear();
            currentImage.getChildren().add(imageSelectionView);
	} catch (Exception ex) {
	    
            error.processError("Whoops something went wrong selection an image.");
	}
      
	});
        
        cancelButton.setOnAction(e -> {
	    hide();
	});
        
        okButton.setOnAction(e -> {
            if(initImageFileName != null)
            {
                ui.getEPGModel().setBannerName(initImageFileName);
                ui.getEPGModel().setBannerPath(initImagePath);
                ui.getEPGModel().updateContent();
                hide();
            }
            else
            {
                error.processError("An image wasn't selected!");
            }
                
        });    
    }
    
}

