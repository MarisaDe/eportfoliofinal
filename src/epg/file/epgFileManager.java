/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.file;

import static epg.StartupConstants.PATH_EPG_PROJECTS;
import epg.model.Component;
import epg.model.EPGModel;
import epg.model.Header;
import epg.model.Image;
import epg.model.Page;
import epg.model.Paragraph;
import epg.model.SlideShow;
import epg.model.Video;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.scene.control.TextField;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_PATH;
import ssm.model.Slide;

/**
 * This class regulates how the ePortfolio is saved as a JSON file.
 * @author Marisa
 */
public class epgFileManager {
    
    public static String JSON_TITLE = "title";
    public static String JSON_NAME = "name";
    public static String JSON_FOOTER = "footer";
    public static String JSON_PAGES = "pages";
    public static String JSON_TYPE = "type";
    public static String JSON_PAGE_TITLE = "pageTitle";
    public static String JSON_PAGE_FONT = "font";
    public static String JSON_PAGE_LAYOUT = "layout";
    public static String JSON_PAGE_COLOR = "color";
    public static String JSON_HEIGHT = "height";
    public static String JSON_WIDTH = "width";
    public static String JSON_FLOAT = "float";
    public static String JSON_FILE_NAME = "file_name";
    public static String JSON_PATH = "path";
    public static String JSON_BANNER_PATH = "banner_path";
    public static String JSON_BANNER_NAME = "banner_name";
    public static String JSON_TEXT = "text";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String JSON_SLIDES = "slides";
    public static String JSON_COMP = "component";
    public static String JSON_ITEMS = "items";
    public static String JSON_ = "pages";
    public static String SLASH = "/";
    int i = 0;
    
    public void saveEPG(EPGModel epgToSave) throws IOException {
	StringWriter sw = new StringWriter();

	//Build the pages array
	JsonArray pagesJsonArray = makePagesJsonArray(epgToSave.getPages());

        //Build 
	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject epgJsonObject = Json.createObjectBuilder()
               .add(JSON_TITLE, epgToSave.getTitle())
               .add(JSON_NAME, epgToSave.getName())
               .add(JSON_FOOTER, epgToSave.getFooter())
               .add(JSON_BANNER_PATH, epgToSave.getBannerPath())
               .add(JSON_BANNER_NAME, epgToSave.getBannerName())
               .add(JSON_PAGES,makePagesJsonArray(epgToSave.getPages()))
	       .build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(epgJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String epgTitle = "" + epgToSave.getTitle();
	String jsonFilePath = PATH_EPG_PROJECTS  + epgTitle + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(epgJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }
        
        
    public void loadEPG(EPGModel epgToLoad, String jsonFilePath) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(jsonFilePath);

	// NOW LOAD THE COURSE
	epgToLoad.reset();
	epgToLoad.setTitle(json.getString(JSON_TITLE));
        epgToLoad.setName(json.getString(JSON_NAME));
        epgToLoad.setFooter(json.getString(JSON_FOOTER));
        epgToLoad.setBannerPath(json.getString(JSON_BANNER_PATH));
        epgToLoad.setBannerName(json.getString(JSON_BANNER_NAME));
        
	JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
	for (int m = 0; m < jsonPagesArray.size(); m++) {
	    JsonObject pageJso = jsonPagesArray.getJsonObject(m);
            Page addPage = new Page(pageJso.getString(JSON_PAGE_TITLE));
            addPage.setLayout(pageJso.getString(JSON_PAGE_LAYOUT));
            addPage.setLayoutColor(pageJso.getString(JSON_PAGE_COLOR));
            addPage.setFontType(pageJso.getString(JSON_PAGE_FONT));
                            
            JsonArray jsonComponentsArray = pageJso.getJsonArray(JSON_COMP);
            
            for(int r = 0; r < jsonComponentsArray.size(); r++)
            {
                JsonObject compJso = jsonComponentsArray.getJsonObject(r);
                
                //if component is an image
                if(compJso.getString(JSON_TYPE).equals("image"))
                {
                  Image image = new Image();  
                  image.setFloat(compJso.getString(JSON_FLOAT));
                  image.setHeight(compJso.getString(JSON_HEIGHT));
                  image.setWidth(compJso.getString(JSON_WIDTH));
                  image.setImagePath(compJso.getString(JSON_PATH));
                  image.setImageFileName(compJso.getString(JSON_FILE_NAME));
                  image.setCaption(compJso.getString(JSON_CAPTION));
                  addPage.addExistingImage(image);
                }
                
                //if component is a video
                else if(compJso.getString(JSON_TYPE).equals("video"))
                {
                  Video video = new Video();
                  video.setVideoPath(compJso.getString(JSON_PATH));
                  video.setVideoFileName(compJso.getString(JSON_NAME));
                  video.setHeight(compJso.getString(JSON_HEIGHT));
                  video.setWidth(compJso.getString(JSON_WIDTH));
                  video.setCaption(compJso.getString(JSON_CAPTION));
                  addPage.addExistingVideo(video);
                }
                
                //if component is a header
                else if(compJso.getString(JSON_TYPE).equals("h"))
                {
                   Header header = new Header(compJso.getString(JSON_TEXT)); 
                   addPage.addExistingHeader(header);
                }
                
                //if component is a paragraph
                else if(compJso.getString(JSON_TYPE).equals("pa"))
                {
                   Paragraph p = new Paragraph(compJso.getString(JSON_TEXT), compJso.getString(JSON_PAGE_FONT));
                   addPage.addExistingParagraph(p);
                }
                //if component is a list
                else if(compJso.getString(JSON_TYPE).equals("li"))
                {
                    epg.model.List list = new epg.model.List();
                    JsonArray jsonListItemsArray = compJso.getJsonArray(JSON_ITEMS);
                    i=0;
                    for(int s = 0; s < jsonListItemsArray.size(); s++)
                    {
                       JsonObject itemJso = jsonListItemsArray.getJsonObject(s);
                       String item =  itemJso.getString(JSON_TEXT+i);
                       TextField txt = new TextField();
                       list.addListItem(item, txt);
                       i++;
                    }
                    i=0;
                    addPage.addExistingList(list);
                    
                }
                //if component is a slide show
                else if(compJso.getString(JSON_TYPE).equals("slideshow"))
                {
                    SlideShow ss = new SlideShow();
                    JsonArray jsonSlidesArray = compJso.getJsonArray(JSON_SLIDES);
                    for(int q = 0; q < jsonSlidesArray.size(); q++)
                    {
                        JsonObject slideJso = jsonSlidesArray.getJsonObject(q);
                        String name = slideJso.getString(JSON_IMAGE_FILE_NAME);
                        String path = slideJso.getString(JSON_IMAGE_PATH);
                        String cap = slideJso.getString(JSON_CAPTION);
                        Slide s = new Slide(name,path,cap); 
                        ss.addSlide(s);
                    }
                    addPage.addExistingSS(ss);
                }
                
                
            }
            epgToLoad.getPages().add(addPage);
	}
    }
        
    // Helper Methods
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);
	ArrayList<String> items = new ArrayList();
	JsonArray jsonArray = json.getJsonArray(arrayName);
	for (JsonValue jsV : jsonArray) {
	    items.add(jsV.toString());
	}
	return items;
    }

    //Parses Page List and turns each Page into a Page JSON object
    //For each page --> makePageJsonObject (which adds global variables) should
    //also go through the Components list in that method
    private JsonArray makePagesJsonArray(List<Page> pages) {
        
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Page page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }

    // Creates a single page object in JSON
    private JsonObject makePageJsonObject(Page page) {
        
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_PAGE_TITLE, page.getTitle())
               .add(JSON_PAGE_LAYOUT, page.getLayout())
               .add(JSON_PAGE_COLOR, page.getLayoutColor())
               .add(JSON_PAGE_FONT, page.getFontType())
               .add(JSON_COMP,makeCompJsonArray(page.getComponents()))
               .build();
        return jso;
    }
    
    //Parses Comp List and turns each Comp into a Comp JSON object
    //For each Component --> make(Comp)JsonObject. This will be determined
    //based on the given component's type.
    private JsonArray makeCompJsonArray(List<Component> comps) {
        
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component comp : comps) {
            
            //header component
            if(comp.getType().equals("h"))
            {
                JsonObject jsoH = makeHeaderObject((epg.model.Header)comp);
                jsb.add(jsoH);
            }
            
            //image component
            else if(comp.getType().equals("image"))
            {
                JsonObject jsoI = makeImageObject((epg.model.Image)comp);
                jsb.add(jsoI);
            } 
            
            //video component
            else if(comp.getType().equals("video"))
            {
                JsonObject jsoV = makeVideoObject((epg.model.Video)comp);
                jsb.add(jsoV);
            }
            
            //paragraph component
            else if(comp.getType().equals("pa"))
            {
                JsonObject jsoP = makeParagraphObject((epg.model.Paragraph)comp);
                jsb.add(jsoP);
            }    
            
            //slideshow component
            else if(comp.getType().equals("slideshow"))
            {
                JsonObject jsoSS = makeSSObject((epg.model.SlideShow)comp);
                jsb.add(jsoSS);
            }   
            
            //List component
            else if(comp.getType().equals("li"))
            {
                JsonObject jsoL = makeListObject((epg.model.List)comp);
                jsb.add(jsoL);
            }              
	}
	JsonArray jA = jsb.build();
	return jA;
    }
    
    
    // Creates the image component for JSON
    private JsonObject makeImageObject(Image image) {
        
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TYPE, image.getType())
               .add(JSON_PATH, image.getImagePath())
               .add(JSON_FILE_NAME, image.getImageFileName())
               .add(JSON_WIDTH, image.getWidth())
               .add(JSON_HEIGHT, image.getHeight())
               .add(JSON_FLOAT, image.getFloat())
               .add(JSON_CAPTION, image.getCaption())
               .build();
        return jso;
    }
    
    
    // Creates the header component for JSON
    private JsonObject makeHeaderObject(Header header) {
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TYPE, header.getType())
               .add(JSON_TEXT, header.getHeader())
               .build();
        return jso;
    }
    
    // Creates the video component for JSON
    private JsonObject makeVideoObject(Video video) {
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TYPE, video.getType())
               .add(JSON_PATH, video.getVideoPath())
               .add(JSON_FILE_NAME, video.getVideoFileName())
               .add(JSON_WIDTH, video.getWidth())
               .add(JSON_HEIGHT, video.getHeight())
               .add(JSON_CAPTION, video.getCaption())
               .build();
        return jso;
    }
    
    // Creates the paragraph component for JSON
    private JsonObject makeParagraphObject(Paragraph p) {
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TYPE, p.getType())
               .add(JSON_PAGE_FONT, p.getFont())
               .add(JSON_TEXT, p.getParagraph())
               .build();
        return jso;
    }
    
    // Creates the SS component for JSON
    private JsonObject makeSSObject(SlideShow s) {
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TYPE, s.getType())
               .add(JSON_SLIDES,makeSlidesJsonArray(s.getSlides()))
               .build();
        return jso;
    }
    
    // Creates an individual slide for the SS component
    private JsonObject makeSlideObject(Slide slide) {
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
		.add(JSON_CAPTION, slide.getCaption())
		.build();
	return jso;
    }
        
        
    //Parses Slide list and turns each Slide into a Slide JSON object
    //For each Slide --> makeSlideObject
    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        
        JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Slide slide : slides) {
	    JsonObject jso = makeSlideObject(slide);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }
    
    // Creates the List Comp for JSON
    private JsonObject makeListObject(epg.model.List list) {
        
        JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TYPE, list.getType())
               .add(JSON_ITEMS, makeListItemsJsonArray(list.getItemList()))
               .build();
        return jso;
    }
    
    // Creates the List Comp for JSON
    private JsonArray makeListItemsJsonArray(List<String> items) {      
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        i=0;
	for (String string : items) {
	    JsonObject jso = makeListItem(string);
	    jsb.add(jso);
            i++;
	}
        i=0;
	JsonArray jA = jsb.build();
	return jA;
    }
    
    // Creates the List Comp for JSON
    private JsonObject makeListItem(String string) {
        
        JsonObject jso = Json.createObjectBuilder()
               .add(JSON_TEXT+i, string)
               .build();
        return jso;
    }
    
    
        
}
