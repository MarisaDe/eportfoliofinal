package epg.error;

import epg.view.ePortfolioGeneratorView;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class provides error messages to the user when the occur. 
 * 
 * @author  Marisa DePasquale
 */
public class EPGErrorHandler {
    // APP UI
    private final ePortfolioGeneratorView ui;
    
    // KEEP THE APP UI FOR LATER
    public EPGErrorHandler(ePortfolioGeneratorView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(String errorType)
    {           
        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        Alert alertDialog = new Alert(AlertType.WARNING, errorType);
	alertDialog.showAndWait();
    }    
}
