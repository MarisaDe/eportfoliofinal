package epg.model;

/**
 * This class represents a single video component on a page in an ePortfolio.
 * @author Marisa
 */
public class Video extends Component{
    
    String videoFileName;
    String videoPath;
    String height;
    String width;
    String caption;

    //Constructer - initializes all image component data
    public Video() {
        super("video");
        videoFileName = null;
	videoPath = null;
	height = null;
        width = null;
        caption = null;
    }    
    
    public Video(String initVideoFileName, String initVideoPath, String initHeight, String initWidth, String initCaption) {
        super("video");
        videoFileName = initVideoFileName;
	videoPath = initVideoPath;
	height = initHeight;
        width = initWidth;
        caption = initCaption;
    }
    
    // Accessor Methods
    public String getVideoFileName() { return videoFileName; }
    public String getVideoPath() { return videoPath; }
    public String getHeight() { return height; }
    public String getWidth() { return width; }
    public String getCaption() { return caption; }
    
    
    // Mutator Methods
    public void setVideoFileName(String initVideoFileName) {videoFileName = initVideoFileName;}
    public void setVideoPath(String initVideoPath) {videoPath = initVideoPath;}
    public void setHeight(String initHeight) {height = initHeight;}
    public void setWidth(String initWidth) {width = initWidth;}
    public void setCaption(String initCaption) {caption = initCaption;}  
    public void setVideo(String initPath, String initFileName) {videoPath = initPath; videoFileName = initFileName; }

}

