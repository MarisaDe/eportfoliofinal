package epg.model;

import epg.view.HeaderView;
import epg.view.ImageView;
import epg.view.ParagraphView;
import epg.view.SlideShowView;
import epg.view.VideoView;
import epg.view.ListView;
import epg.view.ePortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;

/**
 * This class manages all the data associated with an ePortfolio.
 * @author Marisa
 */
public final class EPGModel{
    
    ePortfolioGeneratorView ui;
    String title;
    String name;
    String footer;
    String bannerName;
    String bannerPath;
    ObservableList<Page> pages;
    Page selectedPage;
    Component selectedComp;
    
    
    public EPGModel(ePortfolioGeneratorView initUI)
    {
        title = null;
        name = "null";
        footer = "null";
        bannerName = "null";
        bannerPath = "null";
        ui = initUI;
        pages = FXCollections.observableArrayList();
        selectedComp = null;
        reset();
    }
    
    //Accessor Methods
    public String getName() {return name;} 
    public String getTitle() {return title;} 
    public String getFooter() {return footer;} 
    public String getBannerName() {return bannerName;} 
    public String getBannerPath() {return bannerPath;} 
    public boolean isPageSelected() {return selectedPage != null;}  
    public Component getSelectedComp() {return selectedComp;}  
    public boolean isSelectedPage(Page testPage) {return selectedPage == testPage;}  
    public ObservableList<Page> getPages() {return pages;}
    public Page getSelectedPage() {return selectedPage;}
    public int getSelectedPageIndex() 
    {
        int i = 0;
        for(Page page: pages)
        {
            if(isSelectedPage(page))
            {return i;}
            i++;
        }
        return -1;
    }  
    
    //Mutator Methods
    public void setSelectedPage(Page initSelectedPage) {selectedPage = initSelectedPage;}   
    public void setTitle(String initTitle) { title = initTitle;}
    public void setName(String initName) { name = initName;}
    public void setFooter(String initFooter) { footer = initFooter;}
    public void setSelectedComp(Component comp) { selectedComp = comp;}
    public void setBannerName(String name) {bannerName = name;} 
    public void setBannerPath(String path) {bannerPath = path;} 
    
    //Helper Methods
    public void reset() {
	pages.clear();
	title = null;
	
    }
    
    /**
     * Adds a page to the EPG with the parameter settings.
     */
    public void addPage(Page page) {
	pages.add(page);
	//ui.reloadSlideShowPane();
    }
    
    //removes page based on index
    public void removePage(int index) {
        pages.remove(index);
        //ui.reloadSlideShowPane();
    }
    
    

    public void updateContent()
    {
        ScrollPane scroll = new ScrollPane();
        scroll.setStyle("-fx-background: grey");
        scroll.setMaxWidth(900);
        scroll.setMaxHeight(570);      
        FlowPane frame = new FlowPane();
        frame.setPrefWidth(900);
        
        for(Component comp : getSelectedPage().getComponents())
        {
          if(comp.getType().equals("image")) 
          {
             ImageView imageView = new ImageView((epg.model.Image) comp,ui); 
             frame.getChildren().add(imageView);
          }
          if(comp.getType().equals("video")) 
          {
             VideoView videoView = new VideoView((epg.model.Video) comp,ui); 
             frame.getChildren().add(videoView);
          }
          if(comp.getType().equals("h")) 
          {
             HeaderView headerView = new HeaderView((epg.model.Header) comp,ui); 
             frame.getChildren().add(headerView);
          }
          if(comp.getType().equals("slideshow")) 
          {
             SlideShowView slideshowView = new SlideShowView((epg.model.SlideShow) comp,ui); 
             frame.getChildren().add(slideshowView);
          }
          if(comp.getType().equals("pa")) 
          {
             ParagraphView pView = new ParagraphView((epg.model.Paragraph) comp,ui); 
             frame.getChildren().add(pView);
          }
          if(comp.getType().equals("li")) 
          {
             ListView listView = new ListView((epg.model.List) comp,ui); 
             frame.getChildren().add(listView);
          }
          
          
        }
        scroll.setContent(frame);
        ui.getWorkSpace().getSelectionModel().getSelectedItem().setContent(scroll);
    }

    /**
     * Removes the currently selected slide from the slide show and
     * updates the display.
     */
   /* public void removeSelectedPage() {
	if (isSlideSelected()) {
	    slides.remove(selectedSlide);
	    selectedSlide = null;
	    ui.reloadSlideShowPane();
	}
    }
  */
}

 
