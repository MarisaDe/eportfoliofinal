package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;

/**
 * This class represents an individual page in an ePortfolio.
 * @author Marisa
 */
public class Page {
    
    public Label title;
    String layout;
    String fontType;
    String layoutColor;
    String pageTitle;
    boolean enable;
    ObservableList<Component> components;
    
    //Constructer - initializes data for a page
    
    public Page(String initTitle)
    {
        
        title = new Label(initTitle);
        pageTitle = initTitle;
        enable = true;
        layout = "Right Navigation";
        fontType = "Monda";
        layoutColor = "I'm a Seawolf!";
        components = FXCollections.observableArrayList();
    }
    
    
    //Accessor Methods
    public String getLayout() { return layout; }
    public String getFontType() { return fontType; }
    public String getLayoutColor() { return layoutColor; }
    public String getTitle() { return pageTitle; }
    public Label getTitleLabel(){return title;}
    public ObservableList<Component> getComponents() { return components; }
    public boolean isEnabled() { return enable; }
    
    
    
    //Mutator Methods
    public void setLayout(String initLayout) { layout = initLayout; }
    public void setFontType(String initFontType) { fontType = initFontType; }
    public void setLayoutColor(String initLayoutColor) { layoutColor = initLayoutColor; }
    public void setTitle(String initPageTitle) { pageTitle = initPageTitle; }
    public void setTitleLabel(String initTitle) { title.setText(initTitle); }
    public void isEnabled(boolean initEnable) { enable = initEnable; }
    

    public void addHeader(String initHeader) {
	Header headerToAdd = new Header(initHeader);
	components.add(headerToAdd);
	//ui.reloadEPGPane();
    }
    
    public void addExistingHeader(Header header) {
	components.add(header);
	//ui.reloadEPGPane();
    }
    
    public void addImage(   String initImageFileName,
			    String initImagePath,
                            String initHeight, 
                            String initWidth, 
                            String initFloat,
                            String initCaption) {
	Image imageToAdd = new Image(initImageFileName, initImagePath, initHeight, initWidth, initFloat, initCaption);
	components.add(imageToAdd);
	//ui.reloadEPGPane();
    }
    
    public void addExistingImage(Image image) {
	components.add(image);
	//ui.reloadEPGPane();
    }
    
    public void addVideo(   String initImageFileName,
			    String initImagePath,
                            String initHeight, 
                            String initWidth,
                            String initCaption) {
	Video videoToAdd = new Video(initImageFileName, initImagePath, initHeight, initWidth, initCaption);
	components.add(videoToAdd);
	//ui.reloadEPGPane();
    } 
    
    public void addExistingVideo(Video video) {
	components.add(video);
	//ui.reloadEPGPane();
    } 
    
    public void addExistingSS(SlideShow slideshow) {
	components.add(slideshow);
	//ui.reloadEPGPane();
    } 
    
    public void addExistingParagraph(Paragraph p) {
	components.add(p);
	//ui.reloadEPGPane();
    } 
    
    public void addExistingList(List li) {
	components.add(li);
	//ui.reloadEPGPane();
    } 
    
    public void removeComp(int index) {
	components.remove(index);
	//ui.reloadEPGPane();
    } 
}
