package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;

/**
 * This class represents a single list component on a page in an ePortfolio.
 * @author Marisa
 */
public class List extends Component{
    
    ObservableList<String> listItems;
    ObservableList<TextField> textFieldItems;
    int numOfListItems;
    
    public List()
    {
        super("li");
        numOfListItems = 0;  
        listItems = FXCollections.observableArrayList();
        textFieldItems = FXCollections.observableArrayList();
    }
        
        
    public List(int initNumOfListItems)
    {
        super("li");
        numOfListItems = initNumOfListItems;  
        listItems = FXCollections.observableArrayList();
        textFieldItems = FXCollections.observableArrayList();
    }
    

    public List(ObservableList<String> items, ObservableList<TextField> textItems)
    {
        super("li");
        listItems = FXCollections.observableArrayList();
        textFieldItems = FXCollections.observableArrayList();
        int i = 0;
        for(String string: items)
        {   
            if(string != null)
            {
                listItems.add(string);
                textFieldItems.add(textItems.get(i));
            }
            i++;
        }
        numOfListItems = listItems.size();  
    }
    
    // Accessor Methods
    public int getNumOfListItems(){ return numOfListItems;}
    public ObservableList<TextField> getTextFieldList() {return textFieldItems;}
    public ObservableList<String> getItemList(){ return listItems;}
    
    public void addListItem(String item, TextField field) {
        listItems.add(item);
        textFieldItems.add(field);
	//ui.reloadEPGPane();
    }
    
    public void setListItems(ObservableList<String> items, ObservableList<TextField> textItems){
        
        textFieldItems = FXCollections.observableArrayList();
        listItems = FXCollections.observableArrayList();
        int i = 0;
        for(String string: items)
        {   
            if(string != null)
            {
                listItems.add(string);
                textFieldItems.add(textItems.get(i));
            }
            i++;
        }
        numOfListItems = listItems.size();  
    }
    
    public void setTextFields(ObservableList<TextField> initTxtList){
        textFieldItems = initTxtList;
    }
}
