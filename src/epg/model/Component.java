package epg.model;

/**
 * Super class for component types.
 * @author Marisa
 */
public class Component {
    
    String type;
    Boolean selected;
    
    public Component(String initType){type = initType; selected = false;}  
    public String getType(){return type;}
    public Boolean isSelected(){return selected;}
    public void setType(String initType){ type = initType; }   
    public void setSelected(Boolean selectValue){ selected = selectValue; }  
}
