package epg.model;

/**
 * This class represents a single image component on a page in an ePortfolio.
 * @author Marisa
 */
public class Image extends Component{
    
    String imageFileName;
    String imagePath;
    String height;
    String width;
    String floatPos;
    String caption;
    
    //Constructer - initializes all image component data
    
    public Image()
    {   
        super("image");
    }
    public Image(String initImageFileName, String initImagePath, String initHeight, String initWidth, String initFloat,String initCaption) {
        super("image");
        imageFileName = initImageFileName;
	imagePath = initImagePath;
	height = initHeight;
        width = initWidth;
        floatPos = initFloat;
        caption = initCaption;
    }
    
    
    //Accessor Methods
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getHeight() { return height; }
    public String getWidth() { return width; }
    public String getFloat() { return floatPos; }
    public String getCaption() { return caption; }
    
    
    //Mutator Methods
    public void setImageFileName(String initImageFileName) {imageFileName = initImageFileName;}
    public void setImagePath(String initImagePath) {imagePath = initImagePath;}
    public void setHeight(String initHeight) {height = initHeight;}
    public void setWidth(String initWidth) {width = initWidth;}
    public void setFloat(String initFloat) {floatPos = initFloat;}  
    public void setImage(String initPath, String initFileName) {imagePath = initPath; imageFileName = initFileName; }
    public void setCaption(String initCaption) {caption = initCaption; }

}
