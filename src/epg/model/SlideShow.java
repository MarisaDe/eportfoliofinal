package epg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ssm.model.Slide;

/**
 * This class represents a single slide show component on a page in an ePortfolio.
 * @author Marisa
 */
public class SlideShow extends Component {
    
    String height;
    String width;
    int numOfSlides;
    int numOfCaptions;
    ObservableList<Slide> slides;
    
    public SlideShow ()
    {
        super("slideshow");
        numOfSlides = 0;
        slides = FXCollections.observableArrayList();
    }
        
        
    public SlideShow (int initNumOfSlides)
    {
        super("slideshow");
        numOfSlides = initNumOfSlides;
        slides = FXCollections.observableArrayList();
    }
    
    public SlideShow (ObservableList<Slide> initSlides)
    {
        super("slideshow");
        numOfSlides = initSlides.size();
        slides = initSlides;
    }
    
    //Accessor Methods
    public String getHeight() { return height; }
    public String getWidth() { return width; }
    public int getNumOfSlides() { return numOfSlides; }
    public ObservableList<Slide> getSlides() { return slides; }
    
    //Mutator Methods
    public void setHeight(String initHeight) {height = initHeight;}
    public void setWidth(String initWidth) {width = initWidth;}
    public void setNumOfSlides(int initNumOfSlides) {numOfSlides = initNumOfSlides; }
    
    public void addSlide(Slide slide)
    {
       slides.add(slide);
       numOfSlides++;
    }
    
    public void removeSlide(int index)
    {
       slides.remove(index);
       numOfSlides--;
    }
    
    public void clearSlides()
    {
       slides.removeAll(slides);
       numOfSlides = 0;
    }
}
