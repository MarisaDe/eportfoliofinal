package epg.model;

/**
 * This class represents a single header component on a page in an ePortfolio.
 * @author Marisa
 */
public class Header extends Component{
    
    String header;
    
    //Constructer - initializes all header component data
    public Header(String initHeader) {
        super("h");
        header = initHeader;     
    } 
    
    //Accessor Method
    public String getHeader() { return header; }
    
    //Mutator Method
    public void setHeader(String initHeader) { header = initHeader; }
        
        
        
        
    
}
