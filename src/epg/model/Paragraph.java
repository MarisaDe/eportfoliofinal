package epg.model;

/**
 * This class represents a single paragraph component on a page in an ePortfolio.
 * @author Marisa
 */
public class Paragraph extends Component{
    
    String paragraph;
    String fontType;
    
    public Paragraph(String initParagraph){
        super("pa");
        paragraph = initParagraph;
        fontType = "default";
    }
    
    public Paragraph(String initParagraph, String font){
        super("pa");
        paragraph = initParagraph;
        fontType = font;
    }
        
        
    public String getParagraph(){return paragraph;}
    public String getFont(){return fontType;}
    
    public void setParagraph(String p){paragraph = p;}
    public void setFont(String font){fontType = font;}
    
    
}
