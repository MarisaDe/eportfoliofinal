package epg.view;

import static epg.StartupConstants.CSS_CLASS_IMAGE_COMP;
import static epg.StartupConstants.CSS_CLASS_IMAGE_ICON_COMP;
import static epg.StartupConstants.CSS_CLASS_P_COMP_EXAMPLE;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP_TITLE;
import static epg.StartupConstants.ICON_ADD_IMAGE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.Image;
import epg.model.Page;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.error.ErrorHandler;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * The image component that can be seen/edited/selected
 * @author Marisa
 */
public class ImageView extends HBox{
    //image this component edits/displays in the workspace
    Image imageComp;
    javafx.scene.image.ImageView imageSelectionView;
    
    ePortfolioGeneratorView ui;
    
    //displays basic info about the image
    VBox container;
    VBox infoVBox;
    VBox typeVBox;
    HBox iconAndType;
    Label type;
    Label heightLabel;
    Label widthLabel;
    Label floatLabel;
    Label fileName;
    Label caption;
    Page selectedPage;
    javafx.scene.image.ImageView imageIcon;
     /**
     * This constructor initializes the full UI for this component
     * 
     * @param initImage The image component to be edited/displayed
     * @param initUI The interface it adds it to
     */
    public ImageView(Image initImage, ePortfolioGeneratorView initUI) {
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        String imagePath = "file:" + PATH_ICONS + ICON_ADD_IMAGE;
        imageIcon = new javafx.scene.image.ImageView(imagePath);
        imageIcon.setFitHeight(25);
        imageIcon.setFitWidth(25);
        this.getStylesheets().add(STYLE_SHEET_UI);      
        this.getStyleClass().add(CSS_CLASS_P_COMP_EXAMPLE);
        
        imageComp = initImage;              
        this.setMaxHeight(150);
 
        
        type = new Label("Image");
        type.setUnderline(true);
        heightLabel = new Label("Height: " + imageComp.getHeight());
        widthLabel = new Label("Width: " + imageComp.getWidth());
        floatLabel = new Label("Float: " + imageComp.getFloat()); 
        fileName = new Label("File Name: " + imageComp.getImageFileName());
        caption = new Label("Caption: " + imageComp.getCaption());
        
        container = new VBox();
        container.setPrefWidth(223);
        
        iconAndType = new HBox(5);
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        iconAndType.getChildren().add(imageIcon);
        iconAndType.getChildren().add(type);

        
        
        typeVBox = new VBox();
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        typeVBox.getStyleClass().add(CSS_CLASS_IMAGE_COMP);
        typeVBox.getChildren().add(iconAndType);
    
        infoVBox = new VBox(3);
        infoVBox.getChildren().add(fileName);
        infoVBox.getChildren().add(heightLabel);
        infoVBox.getChildren().add(widthLabel);
        infoVBox.getChildren().add(floatLabel);
        
        
        if(!imageComp.getCaption().equals(""))
        {
            infoVBox.getChildren().add(caption);
        }
        
        
        container.getChildren().add(typeVBox);
        container.getChildren().add(infoVBox);
        
        
        if(imageComp.equals(ui.getEPGModel().getSelectedComp()))
        {
            ui.editCompButton.setDisable(false);
            ui.removeCompButton.setDisable(false);
            this.getStyleClass().clear();
            this.getStyleClass().add(CSS_CLASS_SELECT_COMP);    
            iconAndType.getStyleClass().clear();
            iconAndType.getStyleClass().add(CSS_CLASS_SELECT_COMP_TITLE);
        }
                
                
                
        //Lay out the boxes to create the component
        getChildren().add(container);
        
       
        this.setOnMousePressed(e -> {
            imageComp.setSelected(true);
            for(Page page : ui.getEPGModel().getPages())
            {
                if(!page.equals(selectedPage))
                {
                  for(Component comp: page.getComponents())
                    {
                        comp.setSelected(false);
                    }
                }
            }
            
                for(Component comp: selectedPage.getComponents())
                {
                    if(comp.equals(imageComp))
                    {
                        ui.getEPGModel().setSelectedComp(comp);
                        comp.setSelected(true);                     
                    }
                    else
                        comp.setSelected(false);
                }      
	    ui.getEPGModel().updateContent();
	    });       
    }
    
    
    
    
    
    public void updateImage() {
	String imagePath = imageComp.getImagePath() + SLASH + imageComp.getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    javafx.scene.image.Image slideImage = new javafx.scene.image.Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError(LanguagePropertyType.ERROR_UNEXPECTED);
	}
    }   
}
