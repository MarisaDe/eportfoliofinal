package epg.view;

import static epg.StartupConstants.CSS_CLASS_IMAGE_COMP;
import static epg.StartupConstants.CSS_CLASS_IMAGE_ICON_COMP;
import static epg.StartupConstants.CSS_CLASS_P_COMP_EXAMPLE;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP_TITLE;
import static epg.StartupConstants.ICON_ADD_SLIDESHOW;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.Page;
import epg.model.SlideShow;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * The slide show component that can be seen/edited/selected
 * @author Marisa
 */
public class SlideShowView extends HBox{
    //SlideShow this component edits/displays in the workspace
    SlideShow slideShowComp;
    ePortfolioGeneratorView ui;
    Page selectedPage;
    
    //displays basic info about the image
    VBox container;
    VBox infoVBox;
    VBox typeVBox;
    HBox iconAndType;
    Label type;
    Label slideShow;
    javafx.scene.image.ImageView slideShowIcon;
     /**
     * This constructor initializes the full UI for this component
     * 
     * @param initSlideShow The slideShow component to be edited/displayed
     * @param initUI The work space the header will be added to.
     */
    public SlideShowView(SlideShow initSlideShow, ePortfolioGeneratorView initUI) {
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        String imagePath = "file:" + PATH_ICONS + ICON_ADD_SLIDESHOW;
        slideShowIcon = new javafx.scene.image.ImageView(imagePath);
        slideShowIcon.setFitHeight(25);
        slideShowIcon.setFitWidth(25);
        this.getStylesheets().add(STYLE_SHEET_UI);
        this.getStyleClass().add(CSS_CLASS_P_COMP_EXAMPLE);
        this.setMaxHeight(100);
        slideShowComp = initSlideShow;
        
        type = new Label("Slide Show");
        type.setUnderline(true);
        slideShow = new Label("# of Slides: " + slideShowComp.getNumOfSlides());
        
        container = new VBox();
        container.setPrefWidth(223);
        
        iconAndType = new HBox(5);
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        iconAndType.getChildren().add(slideShowIcon);
        iconAndType.getChildren().add(type);

        
        
        typeVBox = new VBox();
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        typeVBox.getStyleClass().add(CSS_CLASS_IMAGE_COMP);
        typeVBox.getChildren().add(iconAndType);
 
              
        infoVBox = new VBox(3);
        infoVBox.getChildren().add(slideShow);
        
        
        container.getChildren().add(typeVBox);
        container.getChildren().add(infoVBox);
        
        
        if(slideShowComp.equals(ui.getEPGModel().getSelectedComp()))
        {
            ui.editCompButton.setDisable(false);
            ui.removeCompButton.setDisable(false);
            this.getStyleClass().clear();
            this.getStyleClass().add(CSS_CLASS_SELECT_COMP);    
            iconAndType.getStyleClass().clear();
            iconAndType.getStyleClass().add(CSS_CLASS_SELECT_COMP_TITLE);
        }
        
        //Lay out the boxes to create the component
        getChildren().add(container);
        
        this.setOnMousePressed(e -> {
            slideShowComp.setSelected(true);
            for(Page page : ui.getEPGModel().getPages())
            {
                if(!page.equals(selectedPage))
                {
                  for(Component comp: page.getComponents())
                    {
                        comp.setSelected(false);
                    }
                }
            }
            
                for(Component comp: selectedPage.getComponents())
                {
                    if(comp.equals(slideShowComp))
                    {
                        ui.getEPGModel().setSelectedComp(comp);
                        comp.setSelected(true);                     
                    }
                    else
                        comp.setSelected(false);
                }      
	    ui.getEPGModel().updateContent();
	    });
    } 
}
