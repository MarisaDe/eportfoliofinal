package epg.view;

import static epg.StartupConstants.CSS_CLASS_IMAGE_COMP;
import static epg.StartupConstants.CSS_CLASS_IMAGE_ICON_COMP;
import static epg.StartupConstants.CSS_CLASS_P_COMP_EXAMPLE;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP_TITLE;
import static epg.StartupConstants.ICON_HEADER;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.Header;
import epg.model.Page;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * The header component that can be seen/selected/edited
 * @author Marisa
 */
public class HeaderView extends HBox{
    //header this component edits/displays in the workspace
    Header headerComp;
    ePortfolioGeneratorView ui;
    
    //displays basic info about the image
    VBox container;
    VBox infoVBox;
    VBox typeVBox;
    HBox iconAndType;
    Label type;
    Label header;
    javafx.scene.image.ImageView headerIcon;
    Page selectedPage;
     /**
     * This constructor initializes the full UI for this component
     * 
     * @param initHeader The header component to be edited/displayed
     * @param initUI The work space the header will be added to.
     */
    public HeaderView(Header initHeader, ePortfolioGeneratorView initUI) {
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        String imagePath = "file:" + PATH_ICONS + ICON_HEADER;
        headerIcon = new javafx.scene.image.ImageView(imagePath);
        headerIcon.setFitHeight(25);
        headerIcon.setFitWidth(25);
        this.getStylesheets().add(STYLE_SHEET_UI);
        this.getStyleClass().add(CSS_CLASS_P_COMP_EXAMPLE);
        this.setMaxHeight(100);
        headerComp = initHeader;
        
        type = new Label("Header");
        type.setUnderline(true);
        header = new Label("Header: " + headerComp.getHeader());
        
        container = new VBox();
        container.setPrefWidth(223);
        
        iconAndType = new HBox(5);
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        iconAndType.getChildren().add(headerIcon);
        iconAndType.getChildren().add(type);

        
        
        typeVBox = new VBox();
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        typeVBox.getStyleClass().add(CSS_CLASS_IMAGE_COMP);
        typeVBox.getChildren().add(iconAndType);
 
              
        infoVBox = new VBox(3);
        infoVBox.getChildren().add(header);
        
        
        container.getChildren().add(typeVBox);
        container.getChildren().add(infoVBox);
        
        if(headerComp.equals(ui.getEPGModel().getSelectedComp()))
        {  
            ui.editCompButton.setDisable(false);
            ui.removeCompButton.setDisable(false);
            this.getStyleClass().clear();
            this.getStyleClass().add(CSS_CLASS_SELECT_COMP);    
            iconAndType.getStyleClass().clear();
            iconAndType.getStyleClass().add(CSS_CLASS_SELECT_COMP_TITLE);
        }
                
        //Lay out the boxes to create the component
        getChildren().add(container);
        
        this.setOnMousePressed(e -> {
            headerComp.setSelected(true);
            for(Page page : ui.getEPGModel().getPages())
            {
                if(!page.equals(selectedPage))
                {
                  for(Component comp: page.getComponents())
                    {
                        comp.setSelected(false);
                    }
                }
            }
            
                for(Component comp: selectedPage.getComponents())
                {
                    if(comp.equals(headerComp))
                    {
                        ui.getEPGModel().setSelectedComp(comp);
                        comp.setSelected(true);                     
                    }
                    else
                        comp.setSelected(false);
                }      
	    ui.getEPGModel().updateContent();
	    });
        
    }
}
