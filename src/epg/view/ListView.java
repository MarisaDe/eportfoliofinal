package epg.view;

import static epg.StartupConstants.CSS_CLASS_IMAGE_COMP;
import static epg.StartupConstants.CSS_CLASS_IMAGE_ICON_COMP;
import static epg.StartupConstants.CSS_CLASS_P_COMP_EXAMPLE;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP_TITLE;
import static epg.StartupConstants.ICON_LIST;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.Page;
import epg.model.List;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * The list component that can be seen/edited/selected
 * @author Marisa
 */
public class ListView extends HBox{
    //header this component edits/displays in the workspace
    List listComp;
    ePortfolioGeneratorView ui;
    
    //displays basic info about the image
    VBox container;
    VBox infoVBox;
    VBox typeVBox;
    HBox iconAndType;
    Label type;
    Label numOfListItems;
    javafx.scene.image.ImageView listIcon;
    Page selectedPage;
     /**
     * This constructor initializes the full UI for this component
     * 
     * @param initList The list component to be edited/displayed
     * @param initUI The work space the header will be added to.
     */
    public ListView(epg.model.List initList, ePortfolioGeneratorView initUI) {
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        String imagePath = "file:" + PATH_ICONS + ICON_LIST;
        listIcon = new javafx.scene.image.ImageView(imagePath);
        listIcon.setFitHeight(25);
        listIcon.setFitWidth(25);
        this.getStylesheets().add(STYLE_SHEET_UI);
        this.getStyleClass().add(CSS_CLASS_P_COMP_EXAMPLE);
        this.setMaxHeight(100);
        listComp = initList;
        
        type = new Label("List");
        type.setUnderline(true);
        numOfListItems = new Label("# of list items: " + listComp.getNumOfListItems());
        
        container = new VBox();
        container.setPrefWidth(223);
        
        iconAndType = new HBox(5);
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        iconAndType.getChildren().add(listIcon);
        iconAndType.getChildren().add(type);

        
        
        typeVBox = new VBox();
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        typeVBox.getStyleClass().add(CSS_CLASS_IMAGE_COMP);
        typeVBox.getChildren().add(iconAndType);
 
              
        infoVBox = new VBox(3);
        infoVBox.getChildren().add(numOfListItems);
        
        
        container.getChildren().add(typeVBox);
        container.getChildren().add(infoVBox);
        
        if(listComp.equals(ui.getEPGModel().getSelectedComp()))
        {  
            ui.editCompButton.setDisable(false);
            ui.removeCompButton.setDisable(false);
            this.getStyleClass().clear();
            this.getStyleClass().add(CSS_CLASS_SELECT_COMP);    
            iconAndType.getStyleClass().clear();
            iconAndType.getStyleClass().add(CSS_CLASS_SELECT_COMP_TITLE);
        }
                
        //Lay out the boxes to create the component
        getChildren().add(container);
        
        this.setOnMousePressed(e -> {
            listComp.setSelected(true);
            for(Page page : ui.getEPGModel().getPages())
            {
                if(!page.equals(selectedPage))
                {
                  for(Component comp: page.getComponents())
                    {
                        comp.setSelected(false);
                    }
                }
            }
            
                for(Component comp: selectedPage.getComponents())
                {
                    if(comp.equals(listComp))
                    {
                        ui.getEPGModel().setSelectedComp(comp);
                        comp.setSelected(true);                     
                    }
                    else
                        comp.setSelected(false);
                }      
	    ui.getEPGModel().updateContent();
	    });
        
    }
}
