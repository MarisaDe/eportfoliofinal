package epg.view;

import static epg.StartupConstants.CSS_CLASS_IMAGE_COMP;
import static epg.StartupConstants.CSS_CLASS_IMAGE_ICON_COMP;
import static epg.StartupConstants.CSS_CLASS_P_COMP_EXAMPLE;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP;
import static epg.StartupConstants.CSS_CLASS_SELECT_COMP_TITLE;
import static epg.StartupConstants.ICON_PARAGRAPH;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.model.Component;
import epg.model.Page;
import epg.model.Paragraph;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * The paragraph component that can be seen/edited/selected
 * @author Marisa
 */
public class ParagraphView extends HBox{
    //header this component edits/displays in the workspace
    Paragraph pComp;
    ePortfolioGeneratorView ui;
    
    //displays basic info about the image
    VBox container;
    VBox infoVBox;
    VBox typeVBox;
    HBox iconAndType;
    Label type;
    Label paragraph;
    Label fontType;
    javafx.scene.image.ImageView headerIcon;
    Page selectedPage;
     /**
     * This constructor initializes the full UI for this component
     * 
     * @param initHeader The header component to be edited/displayed
     * @param initUI The work space the header will be added to.
     */
    public ParagraphView(Paragraph initParagraph, ePortfolioGeneratorView initUI) {
        
        ui = initUI;
        selectedPage = ui.getEPGModel().getSelectedPage();
        String imagePath = "file:" + PATH_ICONS + ICON_PARAGRAPH;
        headerIcon = new javafx.scene.image.ImageView(imagePath);
        headerIcon.setFitHeight(25);
        headerIcon.setFitWidth(25);
        this.getStylesheets().add(STYLE_SHEET_UI);
        this.getStyleClass().add(CSS_CLASS_P_COMP_EXAMPLE);
        this.setPrefHeight(210);
        pComp = initParagraph;
        
        fontType = new Label("Font: " + pComp.getFont());
        type = new Label("Paragraph");
        type.setUnderline(true);
        
        paragraph = new Label(pComp.getParagraph());
        paragraph.setWrapText(true);
        
        container = new VBox();
        container.setPrefWidth(223);
        container.setPrefHeight(200);
        
        iconAndType = new HBox(5);
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        iconAndType.getChildren().add(headerIcon);
        iconAndType.getChildren().add(type);

        typeVBox = new VBox();
        iconAndType.getStyleClass().add(CSS_CLASS_IMAGE_ICON_COMP);
        typeVBox.getStyleClass().add(CSS_CLASS_IMAGE_COMP);
        typeVBox.getChildren().add(iconAndType);
 
              
        infoVBox = new VBox();
        infoVBox.getChildren().add(fontType);
        infoVBox.getChildren().add(paragraph);
        
        
        container.getChildren().add(typeVBox);
        container.getChildren().add(infoVBox);
        
        if(pComp.equals(ui.getEPGModel().getSelectedComp()))
        {  
            ui.editCompButton.setDisable(false);
            ui.removeCompButton.setDisable(false);
            this.getStyleClass().clear();
            this.getStyleClass().add(CSS_CLASS_SELECT_COMP);    
            iconAndType.getStyleClass().clear();
            iconAndType.getStyleClass().add(CSS_CLASS_SELECT_COMP_TITLE);
        }
                
        //Lay out the boxes to create the component
        getChildren().add(container);
        
        this.setOnMousePressed(e -> {
            pComp.setSelected(true);
            for(Page page : ui.getEPGModel().getPages())
            {
                if(!page.equals(selectedPage))
                {
                  for(Component comp: page.getComponents())
                    {
                        comp.setSelected(false);
                    }
                }
            }
            
                for(Component comp: selectedPage.getComponents())
                {
                    if(comp.equals(pComp))
                    {
                        ui.getEPGModel().setSelectedComp(comp);
                        comp.setSelected(true);                     
                    }
                    else
                        comp.setSelected(false);
                }      
	    ui.getEPGModel().updateContent();
	    });
        
    }
}
