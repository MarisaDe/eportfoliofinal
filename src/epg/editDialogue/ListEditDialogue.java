package epg.editDialogue;

import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.CSS_CLASS_LIST_PAGES_SCROLL;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_HBOX;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_ADD;
import static epg.StartupConstants.ICON_LIST;
import static epg.StartupConstants.ICON_REMOVE;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_ADD_LIST_ITEM;
import static epg.StartupConstants.TOOLTIP_REMOVE_LIST_ITEM;
import epg.error.EPGErrorHandler;
import epg.model.List;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Marisa DePasquale
 */
public class ListEditDialogue extends Stage {
   
    ePortfolioGeneratorView ui;
    EPGErrorHandler error;
    
    TextField reput;
    ObservableList<String> listItems;
    ObservableList<TextField> textFieldItems;
    String input;
    Label enterList;
    Button okButton;
    Button cancelButton;
    Button remove;
    Button add;
    VBox vBox;
    VBox listBox;
    HBox okCancel;
    HBox addRemove;
    HBox containPane;
    ScrollPane textBoxes;
    TextField selected;
    TextField inputList2;
    Page selectedPage;
    
    public ListEditDialogue(ePortfolioGeneratorView initUI, List listToEdit)
    {
      ui = initUI;
      input = null;
      selectedPage = ui.getEPGModel().getSelectedPage();
      selected = null;
      error = new EPGErrorHandler(ui);
      inputList2 = new TextField();
      textFieldItems = listToEdit.getTextFieldList();
      listItems = listToEdit.getItemList();
      enterList = new Label("Edit List Items:");  
      

      okButton = new Button("OK");
      okButton.setMinSize(60, 10);
      cancelButton = new Button("Cancel");
      cancelButton.setMinSize(60, 10);
     
      // box to organize adding and removing list items
      addRemove = new HBox(20);
      addRemove.setAlignment(Pos.CENTER);
      addRemove.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      add = initChildButton(addRemove, ICON_ADD,TOOLTIP_ADD_LIST_ITEM,CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON );       
      remove = initChildButton(addRemove, ICON_REMOVE,TOOLTIP_REMOVE_LIST_ITEM,CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON);
      remove.setDisable(false);
      
      //overall container for displaying the dialogue info
      vBox = new VBox(20); 
      vBox.setMinSize(400, 400);
      vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      
      
      //container for the scroll pane so that its CENTER!
      containPane = new HBox();
      containPane.setPrefWidth(380);
      containPane.setPrefHeight(200);
      containPane.setAlignment(Pos.CENTER);
      
      //scroll pane for the textfield to reside
      textBoxes = new ScrollPane();
      textBoxes.getStyleClass().add(CSS_CLASS_LIST_PAGES_SCROLL);
      textBoxes.setPrefWidth(380);
      textBoxes.setMaxHeight(400);
      
      
      //The box to add in the scroll pane which allows for more text fields
      listBox = new VBox(5);
      listBox.setMinWidth(350);
      //listBox.setPrefHeight(400);
      listBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      listBox.setAlignment(Pos.CENTER); 
      textBoxes.setContent(listBox);
      containPane.getChildren().add(textBoxes);
      
      //The box that organizes the Ok and Cancel buttons
      okCancel = new HBox(100);
      okCancel.setAlignment(Pos.CENTER);
      okCancel.getStyleClass().add(CSS_CLASS_OKCANCEL_HBOX);       
      okCancel.getChildren().add(okButton);
      okCancel.getChildren().add(cancelButton);
      
      int index = 0;
      for(TextField txt : textFieldItems)
      {
        listBox.getChildren().add(txt);
        txt.setText(listItems.get(index));
       
        
        txt.textProperty().addListener(y -> {
            String input2;
            if(txt.getText().equals(""))
            {
              input2 = null;
             }
            else
            {
              input2 = txt.getText();
            }
            int r = textFieldItems.indexOf(txt);
            listItems.set(r, input2);  
         });
         index++;
      }
      
      
      //Add all of the boxes/panes to the container
      vBox.getChildren().add(enterList);
      vBox.getChildren().add(addRemove);
      vBox.getChildren().add(containPane);
      vBox.getChildren().add(okCancel);
      
      cancelButton.setOnAction(e -> {
        this.hide();
      });
      
      
      okButton.setOnAction(e -> {
          
        int numOfNull = 0;
        
        for(String string : listItems)
        {
            if(string == null)
            numOfNull++;
        }
        
        if(numOfNull == listItems.size())
        {
          error.processError("Your list must have at least one list item.");  
        }
        else
        {
            listToEdit.setListItems(listItems, textFieldItems);
            System.out.println(listItems);
            ui.getEPGModel().updateContent();
            ui.saveEPGButton.setDisable(false);
            this.hide();
        }
      });
      
      
      listBox.onMouseClickedProperty().addListener(e -> {
          selected = null;
      });
              
              
              
      add.setOnAction((ActionEvent e) -> {
        remove.setDisable(false);
        inputList2 = new TextField();
        inputList2.setMinWidth(200);
        inputList2.setPromptText("Enter list item here");
        listBox.getChildren().add(inputList2);
        textFieldItems.add(inputList2);
        String getInput = null; 
        listItems.add(getInput);

        inputList2.textProperty().addListener(y -> {
          
          if(inputList2.getText().equals(""))
          {
              input = null;
          }
          else
          {
              input = inputList2.getText();
          }

          int i = textFieldItems.indexOf(inputList2);
          listItems.set(i, input);  
         });
        
        for(TextField txtItem : textFieldItems)
        {
          txtItem.textProperty().addListener(y -> {
          selected = txtItem;
          if(txtItem.getText().equals(""))
          {
              input = null;
          }
          else
          {
              input = txtItem.getText();
          }
                    
            int i = textFieldItems.indexOf(txtItem);
            listItems.set(i, input);  
         });
        }
       });
    
      
    remove.setOnAction((ActionEvent e) -> {
        int lastIndex = textFieldItems.size()-1;
        textFieldItems.remove(lastIndex);
        listItems.remove(lastIndex);
        refresh();
        if(textFieldItems.size() == 1)
        {
            remove.setDisable(true);
        }
        else
            remove.setDisable(false);
    });    
        
      Scene scene = new Scene(vBox);
      scene.getStylesheets().add(STYLE_SHEET_UI);
      this.setTitle("Edit List Component");
      String windowImagePath = "file:" + PATH_ICONS + ICON_LIST;
      Image windowImage = new Image(windowImagePath); 
      this.getIcons().add((windowImage)); 
      setScene(scene);      
    }
    
    
    private void refresh()
      {
        listBox.getChildren().clear();
        int i = 0;
        for(TextField txt : textFieldItems)
        {
                reput = new TextField();
                reput.setMinWidth(200);
                reput.setText(listItems.get(i));
                reput.setPromptText("Enter list item here");
                listBox.getChildren().add(reput);
                //textFieldItems.set(i, reput);
                
          reput.textProperty().addListener(y -> {
            selected = reput;
            String input2;
            
            if(reput.getText().equals(""))
            {
              input2 = null;
             }
            else
            {
              input2 = reput.getText();
            }
            int r = textFieldItems.indexOf(txt);
            listItems.set(r, input2);  
         });
         i++; 
        }
          
      }
          
          
    public final Button initChildButton(
	    HBox hBox, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	hBox.getChildren().add(button);
	return button;
    }  

}

