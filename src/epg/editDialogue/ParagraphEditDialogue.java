/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.editDialogue;

import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.CSS_CLASS_OKCANCEL_HBOX;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_LINK;
import static epg.StartupConstants.ICON_PARAGRAPH;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_LINK;
import epg.error.EPGErrorHandler;
import epg.model.Header;
import epg.model.Page;
import epg.model.Paragraph;
import epg.view.ePortfolioGeneratorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Marisa DePasquale
 */
public class ParagraphEditDialogue extends Stage{
   
    EPGErrorHandler error;
    ePortfolioGeneratorView ui;
    
    String fontType;
    String initP;
    TextArea inputParagraph;
    Label enterParagraph;
    Label font;
    ComboBox fontChoice;
    Button link;
    Button okButton;
    Button cancelButton;
    Button remove;
    Button add;
    VBox vBox;
    HBox paragraphBox;
    HBox okCancel;
    HBox fontBox;
    Page selectedPage;
    
    public ParagraphEditDialogue(ePortfolioGeneratorView initUI, Paragraph pToEdit)
    {
      initP = pToEdit.getParagraph();
      ui = initUI;
      selectedPage = ui.getEPGModel().getSelectedPage();
      enterParagraph = new Label("Enter Paragraph:");  
      font = new Label("Font:");
      
      
    ObservableList<String> fontOptions = FXCollections.observableArrayList(
        "default",
        "Monda",
        "Varela",
        "Sanchez",
        "News Cycle",
        "Roboto Slab"
      );      
     
    
      fontChoice = new ComboBox(fontOptions);
      fontChoice.setValue(selectedPage.getFontType());
      fontType = pToEdit.getFont();
              
      inputParagraph = new TextArea();
      inputParagraph.setText(initP);
      inputParagraph.setMaxSize(300, 260);
      inputParagraph.setWrapText(true);
      okButton = new Button("OK");
      okButton.setMinSize(60, 10);
      cancelButton = new Button("Cancel");
      cancelButton.setMinSize(60, 10);
      
     // addBox = new HBox(20);
     // addBox.setAlignment(Pos.CENTER);
     // add = initChildButton(addBox, ICON_ADD,TOOLTIP_ADD_LIST_ITEM,CSS_CLASS_IMAGE_DIALOGUE );
      
      
      vBox = new VBox(20); 
      vBox.setMinSize(400, 400);
      vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      
      
      fontBox = new HBox(15);
      fontBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      fontBox.getChildren().add(font);
      fontBox.getChildren().add(fontChoice);
      
      link = new Button();
      link = initChildButton(fontBox, ICON_LINK, TOOLTIP_LINK,CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON);
      
      
      paragraphBox = new HBox(5);
      paragraphBox.setAlignment(Pos.CENTER);
     
      
     // remove = initChildButton(listBox, ICON_REMOVE,TOOLTIP_REMOVE_LIST_ITEM,CSS_CLASS_IMAGE_DIALOGUE );
      paragraphBox.getChildren().add(inputParagraph);
     
      
      okCancel = new HBox(100);
      okCancel.setAlignment(Pos.CENTER);
      okCancel.getStyleClass().add(CSS_CLASS_OKCANCEL_HBOX);
          
      okCancel.getChildren().add(okButton);
      okCancel.getChildren().add(cancelButton);
      
      vBox.getChildren().add(enterParagraph);
      vBox.getChildren().add(fontBox);
      vBox.getChildren().add(paragraphBox);
      vBox.getChildren().add(okCancel);
      
      
      
      
      fontChoice.setOnAction(e -> {   
          fontType = fontChoice.getSelectionModel().getSelectedItem().toString(); 
 
        });
      
      inputParagraph.textProperty().addListener(e -> {
        initP = inputParagraph.getText();
      });
              
              
      cancelButton.setOnAction(e -> {
      this.hide();
      });
      
    
      okButton.setOnAction(e -> {
        if(initP != null)
        {
                fontType = fontChoice.getSelectionModel().getSelectedItem().toString();
                pToEdit.setFont(fontType);
                pToEdit.setParagraph(initP); 
                ui.getEPGModel().updateContent();
                ui.saveEPGButton.setDisable(false);
                hide();
        }
        else
        {
            error.processError("No text was entered to make a paragraph!");
        }
                
        });      
              
              
      Scene scene = new Scene(vBox);
      scene.getStylesheets().add(STYLE_SHEET_UI);
      this.setTitle("Edit Paragraph Component");
      String windowImagePath = "file:" + PATH_ICONS + ICON_PARAGRAPH;
      Image windowImage = new Image(windowImagePath); 
      this.getIcons().add((windowImage)); 
      setScene(scene);      
    }
    
    public final Button initChildButton(
	    HBox hBox, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	hBox.getChildren().add(button);
	return button;
    }      
}
