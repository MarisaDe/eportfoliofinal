package epg.editDialogue;

import static epg.StartupConstants.CSS_CLASS_IMAGE_DIALOGUE;
import static epg.StartupConstants.ICON_HEADER;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.STYLE_SHEET_UI;
import epg.error.EPGErrorHandler;
import epg.model.Header;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Marisa
 */
public class HeaderEditDialogue extends Stage{
    
    EPGErrorHandler error;
    ePortfolioGeneratorView ui;
    Page selectedPage;
    String initHeader = null;
    TextField inputHeader;
    Label enterHeader;
    Button okButton;
    Button cancelButton;
    VBox vBox;
    HBox headerBox;
    HBox okCancel;
    
    public HeaderEditDialogue(ePortfolioGeneratorView initUI, Header headerToEdit)
    {
      initHeader = headerToEdit.getHeader();
      ui = initUI;
      selectedPage = ui.getEPGModel().getSelectedPage();
      error = new EPGErrorHandler(ui);
      enterHeader = new Label("Edit Header:");  
      inputHeader = new TextField();
      inputHeader.setPromptText("Edit header here");
      inputHeader.setText(initHeader);
      okButton = new Button("OK");
      okButton.setMinSize(60, 10);
      cancelButton = new Button("Cancel");
      cancelButton.setMinSize(60, 10);
      
      vBox = new VBox(20); 
      vBox.setMinSize(400, 200);
      vBox.getStyleClass().add(CSS_CLASS_IMAGE_DIALOGUE);
      
      headerBox = new HBox(5);
      headerBox.setAlignment(Pos.CENTER);
      headerBox.getChildren().add(enterHeader);
      headerBox.getChildren().add(inputHeader);
      
      okCancel = new HBox(100);
      okCancel.setAlignment(Pos.CENTER);
      okCancel.getChildren().add(okButton);
      okCancel.getChildren().add(cancelButton);
      
      vBox.getChildren().add(headerBox);
      vBox.getChildren().add(okCancel);
      
      
      Scene scene = new Scene(vBox);
      scene.getStylesheets().add(STYLE_SHEET_UI);
      this.setTitle("Edit Header Component");
      String windowImagePath = "file:" + PATH_ICONS + ICON_HEADER;
      Image windowImage = new Image(windowImagePath); 
      this.getIcons().add((windowImage)); 
      setScene(scene);      
      
      initEventHandlers(headerToEdit);  
    }
    
    private void initEventHandlers(Header headerToEdit)
    {
        inputHeader.textProperty().addListener(e -> {
            initHeader = inputHeader.getText();
        });
        
        
        cancelButton.setOnAction(e -> {
            hide();
      });
        
        okButton.setOnAction(e -> {
            if(initHeader != null && !initHeader.equals(""))
            {
                headerToEdit.setHeader(initHeader);
                ui.getEPGModel().updateContent();
                ui.saveEPGButton.setDisable(false);
                hide();
            }
            else
            {
                error.processError("No text was entered to make a header!");
            }
                
        });         
    }
}
