/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import static epg.StartupConstants.CSS_CLASS_WORKSPACE_MODE_TOOLBAR;
import static epg.StartupConstants.CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON;
import static epg.StartupConstants.ICON_PAGE_EDITOR;
import static epg.StartupConstants.ICON_SITE_VIEWER;
import static epg.StartupConstants.PATH_EPG_PROJECTS;
import static epg.StartupConstants.PATH_ICONS;
import static epg.StartupConstants.PATH_IMAGES;
import static epg.StartupConstants.STYLE_SHEET_UI;
import static epg.StartupConstants.TOOLTIP_PAGE_EDITOR;
import static epg.StartupConstants.TOOLTIP_SITE_VIEWER;
import epg.dialogue.ExitDialogue;
import epg.dialogue.SaveDialogue;
import epg.error.EPGErrorHandler;
import epg.file.epgFileManager;
import epg.model.EPGModel;
import epg.model.Page;
import epg.view.ePortfolioGeneratorView;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Marisa
 */
public class FileController {
    
    boolean saved; 
    boolean isSiteViewerOpen;
    private final ePortfolioGeneratorView ui;
    private epgFileManager epgIO;
    
    public FileController(ePortfolioGeneratorView initUI, epgFileManager initIO) {
         
        isSiteViewerOpen = false;
        saved = true;
	ui = initUI;
        epgIO = initIO;
       
    }
    
    public boolean handleSaveEPGRequest() {
        try {
	    //get the EPG to save
	    EPGModel epgToSave = ui.getEPGModel();
	    
            if(ui.getEPGModel().getTitle() == null)
            {
                SaveDialogue saveEPG = new SaveDialogue(ui);
                saveEPG.showAndWait();
            }

            // save it using the title
            epgIO.saveEPG(epgToSave);

            //Mark is as saved
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateFileToolbar(saved);
	    return true;
        } catch (IOException ioe) {
            EPGErrorHandler eH = new EPGErrorHandler(ui);
            eH.processError("Something went wrong");
	    return false;
        }
    }
        
        
    public void handleNewEPGRequest() {
         try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                EPGModel epg = ui.getEPGModel();
		epg.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateFileToolbar(saved);
		ui.updateAllOtherFields(false,true);		
            }
        } catch (IOException ioe) {
            EPGErrorHandler eH = ui.getErrorHandler();
            eH.processError("Unexpected Error Occured");
        }
    }
    
    public void handleLoadEPGRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            EPGErrorHandler eH = ui.getErrorHandler();
            eH.processError("Error loading data file");
        }
    }
 
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork = true; 

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            EPGModel epg = ui.getEPGModel();
          //  epgIO.saveEPG(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
            
            
    public void handleSiteViewRequest() throws MalformedURLException 
    {
        Stage stage = new Stage();
        stage.setTitle(ui.getEPGModel().getSelectedPage().getTitle());
        // Opens dummy file in a new window
        // Make sure to create the toolbar running across the top
        FlowPane workspaceModeToolbar = new FlowPane();  
        workspaceModeToolbar.getStyleClass().add(CSS_CLASS_WORKSPACE_MODE_TOOLBAR);
        workspaceModeToolbar.setPrefWidth(1280);
        Button pageEditButton = initChildButton(workspaceModeToolbar, ICON_PAGE_EDITOR,	TOOLTIP_PAGE_EDITOR,	    CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON, false);
	Button siteViewerButton = initChildButton(workspaceModeToolbar, ICON_SITE_VIEWER,	TOOLTIP_SITE_VIEWER,    CSS_CLASS_WORKSPACE_MODE_TOOLBAR_BUTTON, true);
        
         pageEditButton.setOnAction(e -> {
            stage.hide();
       
        });  
        //Opens webview of that specific index file.
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        browser.setPrefHeight(650);
        browser.setPrefWidth(1280);
        File f = new File("./DummyPage/2.html");
        webEngine.load(f.toURI().toURL().toString());       
        isSiteViewerOpen = true;   
        
        VBox container = new VBox();
        container.getChildren().add(workspaceModeToolbar);
        container.getChildren().add(browser);
        Scene scene = new Scene(container);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        stage.setScene(scene);   
        stage.show();
    }
    
    public void handleExitRequest() throws MalformedURLException 
    {
        ExitDialogue exit = new ExitDialogue(ui);
        
        if(saved == false)
            exit.showAndWait();
        
        System.exit(0);
        
    }
     
    
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser epgFileChooser = new FileChooser();
        epgFileChooser.setInitialDirectory(new File(PATH_EPG_PROJECTS));
        FileChooser.ExtensionFilter jsonFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.JSON");
        epgFileChooser.getExtensionFilters().add(jsonFilter);
        File selectedFile = epgFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		EPGModel epgToLoad = ui.getEPGModel();
                epgIO.loadEPG(epgToLoad, selectedFile.getAbsolutePath());
                ui.setEPGModel(epgToLoad);
                int i = 0;
                for(Page page : ui.getEPGModel().getPages())
                {
                    ui.getEPGModel().setSelectedPage(page);
                    //cant update content cause no pages are in the UI
                    //add pages to UI first but NOT the list
                    //get sitecontroller from UI then use method.
                    ui.listPages.getChildren().add(ui.getSiteController().handleUpdatePageRequest(i));
                    ui.inputName.setText(ui.getEPGModel().getName());
                    ui.inputFooter.setText(ui.getEPGModel().getFooter());
                    ui.getEPGModel().updateContent();
                    i++;
                }
                saved = true;
                ui.updateFileToolbar(saved);
                System.out.println("It works");
            } catch (Exception e) {
                EPGErrorHandler eH = ui.getErrorHandler();
		eH.processError("There is an error with that JSON file and it cannot be parsed.");
            }
        }
    }
    
        public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }   
}
