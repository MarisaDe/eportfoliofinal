
package epg.controller;

import epg.error.EPGErrorHandler;
import epg.view.ePortfolioGeneratorView;
import epg.model.Page;
import java.net.MalformedURLException;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

/**
 * Handles events for the site toolbar and other options
 * @author Marisa
 */
public class SiteController {
    
    EPGErrorHandler error;
    int numOfPages;
    boolean saved;
    private ePortfolioGeneratorView ui;
    
    public SiteController(ePortfolioGeneratorView initUI) {
         
        numOfPages = 1; 
        saved = true;
	ui = initUI;
        EPGErrorHandler error = new EPGErrorHandler(ui);
       
    }
     
    public void handlePageTitleInput(String text)
    {
        ui.getEPGModel().getSelectedPage().setTitle(text);
        ui.getEPGModel().getSelectedPage().setTitleLabel(text);
        ui.workspace.getSelectionModel().getSelectedItem().setText(text);
        ui.workspace.getSelectionModel().getSelectedItem().setId(text);
    }
    
    public void handleFooterInput(String footer)
    {
        ui.getEPGModel().setFooter(footer);
    }
        
    public void handleNameInput(String name)
    {
        ui.getEPGModel().setName(name);
    }
    
    public void handleLayoutChoice(String choice)
    {
        if(ui.getEPGModel().getSelectedPage()!=null)
        {        
            ui.getEPGModel().getSelectedPage().setLayout(choice);
            ui.update.setDisable(true);
        }
    }
    public void handleColorChoice(String choice)
    {
        if(ui.getEPGModel().getSelectedPage()!=null)
        {
            ui.getEPGModel().getSelectedPage().setLayoutColor(choice);
            ui.update.setDisable(true);
        }
    }
    
    public void handleFontChoice(String choice)
    {
        if(ui.getEPGModel().getSelectedPage()!=null)
        {
            ui.getEPGModel().getSelectedPage().setFontType(choice);
            ui.update.setDisable(true);
        }
    }
        
        
    public Label handleAddPageRequest() throws MalformedURLException 
    {
        ui.updateAllOtherFields(false,true);
        ui.inputPageTitle.setDisable(false);
        ui.deletePageButton.setDisable(false);
        Page newPageItem = new Page("Page" + numOfPages);
        ui.getEPGModel().addPage(newPageItem);
        numOfPages++;
        Tab tab = new Tab();
        tab.setText(newPageItem.getTitle());
        tab.setId(newPageItem.getTitle());
        tab.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                if (tab.isSelected()) {
                    int i = ui.getWorkSpace().getSelectionModel().getSelectedIndex();
                    ui.getEPGModel().getPages().get(i).getTitleLabel().setTextFill(Color.web("#0076a3"));
                    ui.getEPGModel().setSelectedComp(null);
                    ui.getEPGModel().updateContent();
                    ui.editCompButton.setDisable(true);
                    ui.removeCompButton.setDisable(true);    
                    ui.colorChoice.setValue(ui.getEPGModel().getSelectedPage().getLayoutColor());
                    ui.layoutChoice.setValue(ui.getEPGModel().getSelectedPage().getLayout());
                    ui.fontChoice.setValue(ui.getEPGModel().getSelectedPage().getFontType());
                    ui.getEPGModel().updateContent();
                }
            }
        });
        FlowPane pane = new FlowPane();      
        tab.setContent(pane);
 
        //adds the tab into the workspace
        ui.getWorkSpace().getTabs().add(tab);  
        
        ui.getEPGModel().setSelectedPage(newPageItem);
        int keeptrack = 0;
            for(Page pages: ui.getEPGModel().getPages()){
                if(ui.getEPGModel().isSelectedPage(pages))
                {
                    pages.getTitleLabel().setTextFill(Color.web("#0076a3"));
                    ui.workspace.getSelectionModel().select(keeptrack);
                }
            else
                {
                    pages.getTitleLabel().setTextFill(Color.web("black"));   
                    keeptrack++;
                }
            }
            
        
        newPageItem.getTitleLabel().setOnMouseEntered(e -> {
            newPageItem.getTitleLabel().setScaleX(1.3);
            newPageItem.getTitleLabel().setScaleY(1.3);
        });
        
        newPageItem.getTitleLabel().setOnMouseExited(e -> {
            newPageItem.getTitleLabel().setScaleX(1);
            newPageItem.getTitleLabel().setScaleY(1);
        });
        
        newPageItem.getTitleLabel().setOnMouseClicked(e -> {
            ui.getEPGModel().setSelectedPage(newPageItem);
            int i = 0;
            for(Page pages: ui.getEPGModel().getPages()){
                if(ui.getEPGModel().isSelectedPage(pages))
                {
                    pages.getTitleLabel().setTextFill(Color.web("#0076a3"));
                    ui.workspace.getSelectionModel().select(i);
                }
                // switch tab to title if it exists
                // if it doesn't exist, open a new tab with proper title
                else
                {
                    pages.getTitleLabel().setTextFill(Color.web("black"));    
                }
                
                i++;
            }
        });
          
        return newPageItem.getTitleLabel();     
    }  
    

        public Label handleUpdatePageRequest(int index) throws MalformedURLException 
    {
        ui.updateAllOtherFields(false,true);
        ui.inputPageTitle.setDisable(false);
        ui.deletePageButton.setDisable(false);
        Page newPageItem = ui.getEPGModel().getPages().get(index);
        numOfPages++;
        Tab tab = new Tab();
        tab.setText(newPageItem.getTitle());
        tab.setId(newPageItem.getTitle());
        tab.setOnSelectionChanged(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                if (tab.isSelected()) {
                    int i = ui.getWorkSpace().getSelectionModel().getSelectedIndex();
                    ui.getEPGModel().getPages().get(i).getTitleLabel().setTextFill(Color.web("#0076a3"));
                    ui.getEPGModel().setSelectedComp(null);
                    ui.getEPGModel().updateContent();
                    ui.editCompButton.setDisable(true);
                    ui.removeCompButton.setDisable(true);    
                    ui.colorChoice.setValue(ui.getEPGModel().getSelectedPage().getLayoutColor());
                    ui.layoutChoice.setValue(ui.getEPGModel().getSelectedPage().getLayout());
                    ui.fontChoice.setValue(ui.getEPGModel().getSelectedPage().getFontType());
                    ui.getEPGModel().updateContent();
                }
            }
        });
        FlowPane pane = new FlowPane();      
        tab.setContent(pane);
 
        //adds the tab into the workspace
        ui.getWorkSpace().getTabs().add(tab);  

        int keeptrack = 0;
            for(Page pages: ui.getEPGModel().getPages()){
                if(ui.getEPGModel().isSelectedPage(pages))
                {
                    pages.getTitleLabel().setTextFill(Color.web("#0076a3"));
                    ui.workspace.getSelectionModel().select(keeptrack);
                }
            else
                {
                    pages.getTitleLabel().setTextFill(Color.web("black"));   
                    keeptrack++;
                }
            }
            
        
        newPageItem.getTitleLabel().setOnMouseEntered(e -> {
            newPageItem.getTitleLabel().setScaleX(1.3);
            newPageItem.getTitleLabel().setScaleY(1.3);
        });
        
        newPageItem.getTitleLabel().setOnMouseExited(e -> {
            newPageItem.getTitleLabel().setScaleX(1);
            newPageItem.getTitleLabel().setScaleY(1);
        });
        
        newPageItem.getTitleLabel().setOnMouseClicked(e -> {
            ui.getEPGModel().setSelectedPage(newPageItem);
            int i = 0;
            for(Page pages: ui.getEPGModel().getPages()){
                if(ui.getEPGModel().isSelectedPage(pages))
                {
                    pages.getTitleLabel().setTextFill(Color.web("#0076a3"));
                    ui.workspace.getSelectionModel().select(i);
                }
                // switch tab to title if it exists
                // if it doesn't exist, open a new tab with proper title
                else
                {
                    pages.getTitleLabel().setTextFill(Color.web("black"));    
                }
                
                i++;
            }
        });
          
        return newPageItem.getTitleLabel();     
    }  
    
        
        
        
    
    public void handleDeletePageRequest() throws MalformedURLException 
    {     
        int i = ui.getWorkSpace().getSelectionModel().getSelectedIndex();   
        int size = ui.getEPGModel().getPages().size()-1;
        
        if(i < size && size > 0){    
            // sets the selected page to the next page
            ui.getEPGModel().setSelectedPage(ui.getEPGModel().getPages().get(i+1));
            ui.getWorkSpace().getTabs().remove( ui.getWorkSpace().getSelectionModel().getSelectedIndex());
            ui.getEPGModel().removePage(i);
        }
        else if( i == size && size == 0 ){
            //if there's only one item in the list, remove it and set selected to null
            ui.getEPGModel().setSelectedPage(null);
            ui.getWorkSpace().getTabs().remove( ui.getWorkSpace().getSelectionModel().getSelectedIndex());
            ui.getEPGModel().removePage(i);
            ui.inputPageTitle.setDisable(true);
            ui.updateAllOtherFields(true,true);
            ui.deletePageButton.setDisable(true);
        }           
        else if(i == size && size > 0){
            // sets the selected page to the previous page
            ui.getEPGModel().setSelectedPage(ui.getEPGModel().getPages().get(i-1));
            ui.getWorkSpace().getTabs().remove( ui.getWorkSpace().getSelectionModel().getSelectedIndex());
            ui.getEPGModel().removePage(i);
        }
    }       
}
