/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.controller;

import epg.dialogue.BannerDialogue;
import epg.dialogue.HeaderDialogue;
import epg.dialogue.ImageDialogue;
import epg.dialogue.ListDialogue;
import epg.dialogue.ParagraphDialogue;
import epg.dialogue.SaveDialogue;
import epg.dialogue.TextDialogue;
import epg.dialogue.VideoDialogue;
import epg.dialogue.VideoEditDialogue;
import epg.editDialogue.HeaderEditDialogue;
import epg.editDialogue.ImageEditDialogue;
import epg.editDialogue.ListEditDialogue;
import epg.editDialogue.ParagraphEditDialogue;
import epg.error.EPGErrorHandler;
import epg.model.Component;
import epg.view.ePortfolioGeneratorView;
import java.net.MalformedURLException;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 *
 * @author Marisa
 */
public class CompController {
    
    EPGErrorHandler error;
    boolean saved;
    private final ePortfolioGeneratorView ui;
    
    public CompController(ePortfolioGeneratorView initUI) {
         
        saved = true;
	ui = initUI;
        error = new EPGErrorHandler(ui);
       
    }
        
        
    public void handleAddImageRequest() throws MalformedURLException 
    {
        ImageDialogue addImage = new ImageDialogue(ui);
        addImage.showAndWait();
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    }
    
    public void handleAddTextRequest() throws MalformedURLException 
    {
        TextDialogue addText = new TextDialogue(ui);
        addText.show();
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    }   
    
    public void handleAddVideoRequest() throws MalformedURLException 
    {
        VideoDialogue addVideo = new VideoDialogue(ui);
        addVideo.showAndWait(); 
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    }      
    public void handleAddParagraphRequest() throws MalformedURLException 
    {
        ParagraphDialogue addParagraph = new ParagraphDialogue(ui);
        addParagraph.show();
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    }
    
    public void handleAddBannerRequest() throws MalformedURLException 
    {
        BannerDialogue addBanner = new BannerDialogue(ui);
        addBanner.show();
        ui.getEPGModel().updateContent();
    }  
        
    public void handleAddListRequest() throws MalformedURLException 
    {
        ListDialogue addList = new ListDialogue(ui);
        addList.show();
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    }     
    public void handleAddHeaderRequest() throws MalformedURLException 
    {
        HeaderDialogue addHeader = new HeaderDialogue(ui);
        addHeader.show();
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    }  
    public void handleSlideShowRequest() throws MalformedURLException 
    {
        SlideShowFileManager initFileManager = new SlideShowFileManager();
        SlideShowMakerView addSS = new SlideShowMakerView(initFileManager,ui);
        ui.getEPGModel().setSelectedComp(null);
        ui.getEPGModel().updateContent();
    } 
    
    public void handleRemoveCompRequest() throws MalformedURLException 
    {
        int i = 0;
        
        if(ui.getEPGModel().getSelectedComp() == null)
        {
          error.processError("No component is selected!");
        }
                
        for(Component comp: ui.getEPGModel().getSelectedPage().getComponents())
        {
            if(comp.equals(ui.getEPGModel().getSelectedComp()))
            {
                ui.getEPGModel().getSelectedPage().removeComp(i);
                ui.getEPGModel().updateContent();
            }
        i++;
        }
        
    } 
    
    public void handleEditCompRequest() 
    {
        Component selectedComp = ui.getEPGModel().getSelectedComp();  
        
        if(selectedComp == null)
        {
          error.processError("No component is selected!");
        }
        else if(selectedComp.getType().equals("image"))
        {
            ImageEditDialogue imageEdit = new ImageEditDialogue(ui, (epg.model.Image)selectedComp);
            imageEdit.showAndWait();
            ui.getEPGModel().updateContent();
        }
        
        else if(selectedComp.getType().equals("video"))
        {
            VideoEditDialogue videoEdit = new VideoEditDialogue(ui, (epg.model.Video)selectedComp);
            videoEdit.showAndWait();
            ui.getEPGModel().updateContent();
        }  
        
        else if(selectedComp.getType().equals("h"))
        {
            HeaderEditDialogue headerEdit = new HeaderEditDialogue(ui, (epg.model.Header)selectedComp);
            headerEdit.showAndWait();
            ui.getEPGModel().updateContent();
        }   
        else if(selectedComp.getType().equals("slideshow"))
        {
            SlideShowFileManager initFileManager = new SlideShowFileManager();
            SlideShowMakerView editSS = new SlideShowMakerView(initFileManager,ui,(epg.model.SlideShow)selectedComp);
            ui.getEPGModel().updateContent();
        } 
        else if(selectedComp.getType().equals("pa"))
        {
            ParagraphEditDialogue pEdit = new ParagraphEditDialogue(ui, (epg.model.Paragraph)selectedComp);
            pEdit.showAndWait();
            ui.getEPGModel().updateContent();
        } 
        else if(selectedComp.getType().equals("li"))
        {
            ListEditDialogue liEdit = new ListEditDialogue(ui, (epg.model.List)selectedComp);
            liEdit.showAndWait();
            ui.getEPGModel().updateContent();
        } 
        
        
    } 
}
